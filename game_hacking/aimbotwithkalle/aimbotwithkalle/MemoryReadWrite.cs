﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;

namespace aimbotwithkalle
{
    public class MemoryReadWrite
    {
        [DllImport("kernel32.dll")]
        public static extern int WriteProcessMemory(IntPtr Handle, long Address, byte[] buffer, int Size, int BytesWritten = 0);
        [DllImport("kernel32.dll")]
        public static extern int ReadProcessMemory(IntPtr Handle, long Address, byte[] buffer, int Size, int BytesRead = 0);

        public IntPtr pHandle;
        public string ExeName { get; set; }
        public string ProcessName { get; set; }
        public long BaseAddress { get; set; }

        public IntPtr GetProcessHandle()
        {
            try
            {
                Process[] ProcList = Process.GetProcessesByName(ExeName);

                pHandle = ProcList[0].Handle;

                return pHandle;
            }
            catch
            {
                return IntPtr.Zero;
            }
        }

        public long GetBaseAddress(string ModuleName)
        {
            try
            {
                Process[] processes = Process.GetProcessesByName(ExeName);

                long baseAddress = processes[0].MainModule.BaseAddress.ToInt64();

                return baseAddress;
            }
            catch
            {
                return 0;
            }
        }

        public ProcessModule GetProcessModule()
        {
            try
            {
                Process[] processes = Process.GetProcessesByName(ExeName);

                ProcessModule module = processes[0].MainModule;

                return module;
            }
            catch
            {
                return null;
            }
        }

        public long GetPointerAddress(long Pointer, int[] Offset = null)
        {
            byte[] Buffer = new byte[8];

            ReadProcessMemory(GetProcessHandle(), Pointer, Buffer, Buffer.Length);

            if (Offset != null)
            {
                for (int x = 0; x < (Offset.Length - 1); x++)
                {
                    Pointer = BitConverter.ToInt32(Buffer, 0) + Offset[x]; //whyyyy???? für 8byte float hier wieder to int64
                    ReadProcessMemory(GetProcessHandle(), Pointer, Buffer, Buffer.Length);
                }

                Pointer = BitConverter.ToInt32(Buffer, 0) + Offset[Offset.Length - 1];
            }

            return Pointer;
        }

        public byte[] GetMemoryDump()
        {
            ProcessModule module = GetProcessModule();

            uint memorySize = (uint)module.ModuleMemorySize;
            byte[] memoryDump = new byte[memorySize];

            ReadProcessMemory(GetProcessHandle(), GetBaseAddress(module.ModuleName), memoryDump, memoryDump.Length);

            return memoryDump;
        }

        public bool CheckPattern(string pattern, byte[] array)
        {
            string[] strBytes = pattern.Split(' ');
            int x = 0;

            foreach (byte b in array)
            {
                if (strBytes[x] == "?" || strBytes[x] == "??")
                {
                    x++;
                }
                else if (byte.Parse(strBytes[x], NumberStyles.HexNumber) == b)
                {
                    x++;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        public int ScanForPattern(string pattern)
        {
            byte[] memoryDump = GetMemoryDump();
            string[] patternBytes = pattern.Split(' ');

            IntPtr baseAdress = GetProcessModule().BaseAddress;

            MessageBox.Show("BaseAdress: " + baseAdress.ToString("X"));

            try
            {
                for (int y = 0; y < memoryDump.Length; y++)
                {
                    if (memoryDump[y] == byte.Parse(patternBytes[0], NumberStyles.HexNumber))
                    {
                        byte[] checkArray = new byte[patternBytes.Length];
                        for (int x = 0; x < patternBytes.Length; x++)
                        {
                            checkArray[x] = memoryDump[y + x];
                        }
                        if (CheckPattern(pattern, checkArray))
                        {
                            return y;
                        }
                        else
                        {
                            y += patternBytes.Length - (patternBytes.Length / 2);
                        }
                    }
                }
            }

            catch (Exception)
            {
                return 0;
            }

            return 0;
        }

        public void WriteBytes(long Address, byte[] Bytes)
        {
            WriteProcessMemory(GetProcessHandle(), Address, Bytes, Bytes.Length);
        }
        public void WriteFloat(long Address, float Value)
        {
            WriteProcessMemory(GetProcessHandle(), Address, BitConverter.GetBytes(Value), 4);
        }
        public void WriteDouble(long Address, double Value)
        {
            WriteProcessMemory(GetProcessHandle(), Address, BitConverter.GetBytes(Value), 8);
        }
        public void WriteInteger(long Address, int Value, int size)
        {
            WriteProcessMemory(GetProcessHandle(), Address, BitConverter.GetBytes(Value), size);
        }
        public void WriteString(long Address, string String)
        {
            byte[] Buffer = new ASCIIEncoding().GetBytes(String);
            WriteProcessMemory(GetProcessHandle(), Address, Buffer, Buffer.Length);
        }

        public byte[] ReadBytes(long Address, int Length)
        {
            byte[] Buffer = new byte[Length];
            ReadProcessMemory(GetProcessHandle(), Address, Buffer, Length);
            return Buffer;
        }
        public float ReadFloat(long Address)
        {
            byte[] Buffer = new byte[4]; ;
            ReadProcessMemory(GetProcessHandle(), Address, Buffer, 4);
            return BitConverter.ToSingle(Buffer, 0);
        }
        public double ReadDouble(long Address)
        {
            byte[] Buffer = new byte[8];
            ReadProcessMemory(GetProcessHandle(), Address, Buffer, 8);
            return BitConverter.ToDouble(Buffer, 0);
        }
        public int ReadInteger(long Address, int Length)
        {
            byte[] Buffer = new byte[Length];
            ReadProcessMemory(GetProcessHandle(), Address, Buffer, Length);
            return BitConverter.ToInt32(Buffer, 0);
        }
        public string ReadString(long Address, int size)
        {
            byte[] Buffer = new byte[size]; ;
            ReadProcessMemory(GetProcessHandle(), Address, Buffer, size);
            return new ASCIIEncoding().GetString(Buffer);
        }
        public long ReadPointer(long Address)
        {
            byte[] Buffer = new byte[8];
            ReadProcessMemory(GetProcessHandle(), Address, Buffer, Buffer.Length);
            return BitConverter.ToInt64(Buffer, 0);
        }
    }
}
