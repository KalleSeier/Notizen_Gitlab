﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
namespace aimbotwithkalle
{
   public class CheatFunctions : MemoryReadWrite
    {
        #region Offsets

        static int MAX_PLAYER_AMMO = 20;

        int PlayerBase = 0x0017E0A8;

        int[] Offset_PlayerHealth = { 0xEC };
        int[] Offset_Player_x = { 0x2C}; //gilt auch für bot von EnemyBase1
        int[] Offset_Player_y = { 0x28 };
        int[] Offset_Player_z = { 0x30};
        int[] Offset_Player_Ammunition = { 0x140 };
        int[] Offset_Player_horizontal_View = { 0x34 };
        int[] Offset_Player_vertical_View = { 0x38 };



        int EnemyBase1 = 0x00191FCC;

        int[] Offset_Enemy_Health = { 0xEC };
        int[] Enemy_Offset = {0x4}; //dass erhöhen für jeden Bot. fängt mit 0x0 an.
        int[] Offset_Enemy_Name = { 0x205 };
        int[] Offset_Enemy_X = { 0x0 };
        #endregion


        public CheatFunctions(string exeName, string processName)
        {
            ExeName = exeName;
            ProcessName = processName;
            BaseAddress = GetBaseAddress(ProcessName);
            pHandle = GetProcessHandle();
        }

        public bool IsGameRunning()
        {
            Process[] process = Process.GetProcessesByName(ExeName);
            if (process.Length > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public long Player_Get_Health()
        {
            long pointer = GetPointerAddress(BaseAddress + PlayerBase, Offset_PlayerHealth);

            return ReadInteger(pointer, 4);
        }

        public float Player_Get_X()
        {
            long pointer = GetPointerAddress(BaseAddress + PlayerBase, Offset_Player_x);

            return ReadFloat(pointer);
        }

        public float Player_Get_Y()
        {
            long pointer = GetPointerAddress(BaseAddress + PlayerBase, Offset_Player_y);

            
            return ReadFloat(pointer);
        }

        public float Player_Get_Z()
        {
            long pointer = GetPointerAddress(BaseAddress + PlayerBase, Offset_Player_z);

            return ReadFloat(pointer);
        }

        public void Player_Health_Freeze(int health)
        {
            long pointer = GetPointerAddress(BaseAddress + PlayerBase, Offset_PlayerHealth);
            WriteInteger(pointer, health,4);
        }
        public void Player_Set_Health(int health)
        {
            long pointer = GetPointerAddress(BaseAddress + PlayerBase, Offset_PlayerHealth);
            WriteInteger(pointer, health, 4);
        }

        public void Player_Set_Ammunition()
        {
            long pointer = GetPointerAddress(BaseAddress + PlayerBase, Offset_Player_Ammunition);
            WriteInteger(pointer, MAX_PLAYER_AMMO, 4);
        }

        public int Enemy_Get_Health()
        {
            long pointer = GetPointerAddress(BaseAddress + EnemyBase1);
            int x = ReadInteger(pointer, 4);
            pointer = GetPointerAddress(x, Offset_Enemy_Health);

            return ReadInteger(pointer, 4);
        }
        public double Enemy_Get_Closest_Enemy(int Number_of_Enemys,float playerx,float playery,float playerz)  //muss noch gemacht werden
        {
            int offset = 0x0;
            int i = 0;
            Number_of_Enemys -= 1;
            float Bot_Position_x = 0;
            float Bot_Position_y = 0;
            float Bot_Position_z = 0;
            double c;
            double Distance_to_Enemy = 10000;
            while (Number_of_Enemys >= i)
            {
                long pointer = GetPointerAddress(BaseAddress + EnemyBase1);
                int x = ReadInteger(pointer, 4);
                pointer = GetPointerAddress(x+ offset, Offset_Player_x);
                Bot_Position_x = ReadFloat(pointer);
                pointer = GetPointerAddress(x + offset, Offset_Player_y);
                Bot_Position_y = ReadFloat(pointer);
                pointer = GetPointerAddress(x + offset, Offset_Player_z);
                Bot_Position_z = ReadFloat(pointer);
                offset += Enemy_Offset[0];
                Bot_Position_x -= playerx;
                Bot_Position_x *= Bot_Position_x;
                Bot_Position_y -= playery;
                Bot_Position_y *= Bot_Position_y;
                c = Math.Sqrt(Bot_Position_x + Bot_Position_y); 
                if(Distance_to_Enemy >= c)
                {
                    Distance_to_Enemy = c;
                }
                i++;
            }
            return Distance_to_Enemy; 
        }

        public double Enemy_Get_Closest_Enemy_Alive(int Number_of_Enemys, float playerx, float playery, float playerz, ref int closest_Enemy)  
        {
            int offset = 0x0;
            int i = 0;
            Number_of_Enemys -= 1;
            float Bot_Position_x = 0;
            float Bot_Position_y = 0;
            float Bot_Position_z = 0;
            int BotHealth;
            double c;
            double Distance_to_Enemy = 10000;
            while (Number_of_Enemys >= i) 
            {
                long pointer = GetPointerAddress(BaseAddress + EnemyBase1);
                int x = ReadInteger(pointer, 4);
                pointer = GetPointerAddress(x + offset, Offset_Player_x);
                Bot_Position_x = ReadFloat(pointer);
                pointer = GetPointerAddress(x + offset, Offset_Enemy_Health);
                BotHealth = ReadInteger(pointer,4);
                pointer = GetPointerAddress(x + offset, Offset_Player_y);
                Bot_Position_y = ReadFloat(pointer);
                pointer = GetPointerAddress(x + offset, Offset_Player_z);
                Bot_Position_z = ReadFloat(pointer);
                offset += Enemy_Offset[0];
                Bot_Position_x -= playerx;
                Bot_Position_x *= Bot_Position_x;
                Bot_Position_y -= playery;
                Bot_Position_y *= Bot_Position_y;
                c = Math.Sqrt(Bot_Position_x + Bot_Position_y);
                if (Distance_to_Enemy >= c && BotHealth >= 0)
                {
                    Distance_to_Enemy = c;
                    closest_Enemy = i;
                }
                i++;
            }
            return Distance_to_Enemy;
        }
        public int Enemy_Get_Count2()
        {
            
            int number = 0;
            int health = 0;
            int offset = 0x0;
            int look = 0x0;
            int offsetforlook = 0x8;
            int test1, test2, test3;
            do
            {
                #region Health von Bots
                health = 0;
                long pointer = GetPointerAddress(BaseAddress + EnemyBase1);
                int x = ReadInteger(pointer, 4);
                pointer = GetPointerAddress(x + offset, Offset_Enemy_Health);
                offset += Enemy_Offset[0]; //+04    
                health = ReadInteger(pointer + look, 4);
                look += offsetforlook;
                test1 = ReadInteger(pointer + look, 4);
                look += offsetforlook;
                test2 = ReadInteger(pointer + look, 4);
                look += offsetforlook;
                test3 = ReadInteger(pointer + look, 4);
                number++;
                look = 0x0;
                #endregion
            }
            while (health != 0 && test1 != 0 && test1 <=100 && test2 != 0 && test3 != 0);

            return number - 1;
        }

        public void Player_Aim_at_Enemy(int offset,float playerx,float playery,float playerz)
        {
            offset *= Enemy_Offset[0];
            long pointer = GetPointerAddress(BaseAddress + EnemyBase1);
            int x = ReadInteger(pointer, 4);
            pointer = GetPointerAddress(x + offset, Offset_Player_x); //hier dann auf die Bot daten eingehen
            float bot_x = ReadFloat(pointer);
            pointer = GetPointerAddress(x + offset, Offset_Player_y); //hier dann auf die Bot daten eingehen
            float bot_y = ReadFloat(pointer);
            pointer = GetPointerAddress(x + offset, Offset_Player_z); //hier dann auf die Bot daten eingehen
            float bot_z = ReadFloat(pointer);
            long pointervertical = GetPointerAddress(BaseAddress + PlayerBase, Offset_Player_vertical_View);
            float vertical = ReadFloat(pointer);
            long pointerhorizontal = GetPointerAddress(BaseAddress + PlayerBase, Offset_Player_horizontal_View);
            float horizontal = ReadFloat(pointer);

            float deltax = bot_x - playerx;
            float deltay = bot_y - playery;
            float deltaz = bot_z - playerz;

            float deltaVecLength = (float)Math.Sqrt(deltax*deltax+deltay*deltay+deltaz*deltaz); //länge von unserem standpunkt zum gegnerischen.
            float pitch = (float)Math.Asin(deltaz / deltaVecLength) * (180 / (float)Math.PI);
            float yaw = -(float)Math.Atan2(deltay, deltax) * (180 / (float)Math.PI)+180; 

            
            WriteFloat(pointervertical, pitch);
            WriteFloat(pointerhorizontal, yaw);
        }
    }
}
