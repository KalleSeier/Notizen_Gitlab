namespace Chessblind
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            Variables.List_squares = FillList_Picturebox();
            Variables.List_images = FillList_ImagesList();
        }
        public List<PictureBox> FillList_Picturebox()
        {
            #region add_pbx
            List<PictureBox> list = new List<PictureBox>();
            list.Add(pbx_square_1);
            list.Add(pbx_square_2);
            list.Add(pbx_square_3);
            list.Add(pbx_square_4);
            list.Add(pbx_square_5);
            list.Add(pbx_square_6);
            list.Add(pbx_square_7);
            list.Add(pbx_square_8);
            list.Add(pbx_square_9);
            list.Add(pbx_square_10);
            list.Add(pbx_square_11);
            list.Add(pbx_square_12);
            list.Add(pbx_square_13);
            list.Add(pbx_square_14);
            list.Add(pbx_square_15);
            list.Add(pbx_square_16);
            list.Add(pbx_square_17);
            list.Add(pbx_square_18);
            list.Add(pbx_square_19);
            list.Add(pbx_square_21);
            list.Add(pbx_square_22);
            list.Add(pbx_square_23);
            list.Add(pbx_square_24);
            list.Add(pbx_square_25);
            list.Add(pbx_square_26);
            list.Add(pbx_square_27);
            list.Add(pbx_square_28);
            list.Add(pbx_square_29);
            list.Add(pbx_square_31);
            list.Add(pbx_square_32);
            list.Add(pbx_square_33);
            list.Add(pbx_square_34);
            list.Add(pbx_square_35);
            list.Add(pbx_square_36);
            list.Add(pbx_square_37);
            list.Add(pbx_square_38);
            list.Add(pbx_square_39);
            list.Add(pbx_square_40);
            list.Add(pbx_square_41);
            list.Add(pbx_square_42);
            list.Add(pbx_square_43);
            list.Add(pbx_square_44);
            list.Add(pbx_square_45);
            list.Add(pbx_square_46);
            list.Add(pbx_square_47);
            list.Add(pbx_square_48);
            list.Add(pbx_square_49);
            list.Add(pbx_square_50);
            list.Add(pbx_square_51);
            list.Add(pbx_square_52);
            list.Add(pbx_square_53);
            list.Add(pbx_square_54);
            list.Add(pbx_square_55);
            list.Add(pbx_square_56);
            list.Add(pbx_square_57);
            list.Add(pbx_square_58);
            list.Add(pbx_square_59);
            list.Add(pbx_square_60);
            list.Add(pbx_square_61);
            list.Add(pbx_square_62);
            list.Add(pbx_square_63);
            list.Add(pbx_square_64);
            return list;
#endregion
        }
        public List<Image> FillList_ImagesList()
        {
            #region add_img
            List<Image> list = new List<Image>();
            //list.Add(new Bitmap("w"));
            //list.Add(new Bitmap("b"));
            //list.Add(new Bitmap("wrows"));
            //list.Add(new Bitmap("wknows"));
            //list.Add(new Bitmap("wbonws"));
            //list.Add(new Bitmap("wkonws"));
            //list.Add(new Bitmap("wqows"));
            //list.Add(new Bitmap("wrobs"));
            //list.Add(new Bitmap("wknobs"));
            //list.Add(new Bitmap("wbobs"));
            //list.Add(new Bitmap("wkobs"));
            //list.Add(new Bitmap("wqobs"));
            //list.Add(new Bitmap("brobs"));
            //list.Add(new Bitmap("bknobs"));
            //list.Add(new Bitmap("bbobs"));
            //list.Add(new Bitmap("bkobs"));
            //list.Add(new Bitmap("bqobs"));
            return list;
        }
        #endregion
    }
}