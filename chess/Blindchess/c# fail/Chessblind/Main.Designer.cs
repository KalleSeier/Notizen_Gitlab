﻿namespace Chessblind
{
    partial class Main
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.pbx_square_1 = new System.Windows.Forms.PictureBox();
            this.pbx_square_2 = new System.Windows.Forms.PictureBox();
            this.pbx_square_4 = new System.Windows.Forms.PictureBox();
            this.pbx_square_3 = new System.Windows.Forms.PictureBox();
            this.pbx_square_8 = new System.Windows.Forms.PictureBox();
            this.pbx_square_7 = new System.Windows.Forms.PictureBox();
            this.pbx_square_6 = new System.Windows.Forms.PictureBox();
            this.pbx_square_5 = new System.Windows.Forms.PictureBox();
            this.pbx_square_15 = new System.Windows.Forms.PictureBox();
            this.pbx_square_14 = new System.Windows.Forms.PictureBox();
            this.pbx_square_13 = new System.Windows.Forms.PictureBox();
            this.pbx_square_12 = new System.Windows.Forms.PictureBox();
            this.pbx_square_11 = new System.Windows.Forms.PictureBox();
            this.pbx_square_10 = new System.Windows.Forms.PictureBox();
            this.pbx_square_9 = new System.Windows.Forms.PictureBox();
            this.pbx_square_16 = new System.Windows.Forms.PictureBox();
            this.pbx_square_24 = new System.Windows.Forms.PictureBox();
            this.pbx_square_23 = new System.Windows.Forms.PictureBox();
            this.pbx_square_22 = new System.Windows.Forms.PictureBox();
            this.pbx_square_21 = new System.Windows.Forms.PictureBox();
            this.pbx_square_20 = new System.Windows.Forms.PictureBox();
            this.pbx_square_19 = new System.Windows.Forms.PictureBox();
            this.pbx_square_18 = new System.Windows.Forms.PictureBox();
            this.pbx_square_17 = new System.Windows.Forms.PictureBox();
            this.pbx_square_40 = new System.Windows.Forms.PictureBox();
            this.pbx_square_39 = new System.Windows.Forms.PictureBox();
            this.pbx_square_38 = new System.Windows.Forms.PictureBox();
            this.pbx_square_37 = new System.Windows.Forms.PictureBox();
            this.pbx_square_36 = new System.Windows.Forms.PictureBox();
            this.pbx_square_35 = new System.Windows.Forms.PictureBox();
            this.pbx_square_34 = new System.Windows.Forms.PictureBox();
            this.pbx_square_33 = new System.Windows.Forms.PictureBox();
            this.pbx_square_31 = new System.Windows.Forms.PictureBox();
            this.pbx_square_30 = new System.Windows.Forms.PictureBox();
            this.pbx_square_29 = new System.Windows.Forms.PictureBox();
            this.pbx_square_28 = new System.Windows.Forms.PictureBox();
            this.pbx_square_27 = new System.Windows.Forms.PictureBox();
            this.pbx_square_26 = new System.Windows.Forms.PictureBox();
            this.pbx_square_25 = new System.Windows.Forms.PictureBox();
            this.pbx_square_32 = new System.Windows.Forms.PictureBox();
            this.pbx_square_56 = new System.Windows.Forms.PictureBox();
            this.pbx_square_55 = new System.Windows.Forms.PictureBox();
            this.pbx_square_54 = new System.Windows.Forms.PictureBox();
            this.pbx_square_53 = new System.Windows.Forms.PictureBox();
            this.pbx_square_52 = new System.Windows.Forms.PictureBox();
            this.pbx_square_51 = new System.Windows.Forms.PictureBox();
            this.pbx_square_50 = new System.Windows.Forms.PictureBox();
            this.pbx_square_49 = new System.Windows.Forms.PictureBox();
            this.pbx_square_47 = new System.Windows.Forms.PictureBox();
            this.pbx_square_46 = new System.Windows.Forms.PictureBox();
            this.pbx_square_45 = new System.Windows.Forms.PictureBox();
            this.pbx_square_44 = new System.Windows.Forms.PictureBox();
            this.pbx_square_43 = new System.Windows.Forms.PictureBox();
            this.pbx_square_42 = new System.Windows.Forms.PictureBox();
            this.pbx_square_41 = new System.Windows.Forms.PictureBox();
            this.pbx_square_48 = new System.Windows.Forms.PictureBox();
            this.pbx_square_63 = new System.Windows.Forms.PictureBox();
            this.pbx_square_62 = new System.Windows.Forms.PictureBox();
            this.pbx_square_61 = new System.Windows.Forms.PictureBox();
            this.pbx_square_60 = new System.Windows.Forms.PictureBox();
            this.pbx_square_59 = new System.Windows.Forms.PictureBox();
            this.pbx_square_58 = new System.Windows.Forms.PictureBox();
            this.pbx_square_57 = new System.Windows.Forms.PictureBox();
            this.pbx_square_64 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_64)).BeginInit();
            this.SuspendLayout();
            // 
            // pbx_square_1
            // 
            this.pbx_square_1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_1.BackgroundImage")));
            this.pbx_square_1.Location = new System.Drawing.Point(0, 1);
            this.pbx_square_1.Name = "pbx_square_1";
            this.pbx_square_1.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_1.TabIndex = 0;
            this.pbx_square_1.TabStop = false;
            // 
            // pbx_square_2
            // 
            this.pbx_square_2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_2.BackgroundImage")));
            this.pbx_square_2.Location = new System.Drawing.Point(102, 1);
            this.pbx_square_2.Name = "pbx_square_2";
            this.pbx_square_2.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_2.TabIndex = 1;
            this.pbx_square_2.TabStop = false;
            // 
            // pbx_square_4
            // 
            this.pbx_square_4.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_4.BackgroundImage")));
            this.pbx_square_4.Location = new System.Drawing.Point(306, 1);
            this.pbx_square_4.Name = "pbx_square_4";
            this.pbx_square_4.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_4.TabIndex = 3;
            this.pbx_square_4.TabStop = false;
            // 
            // pbx_square_3
            // 
            this.pbx_square_3.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_3.BackgroundImage")));
            this.pbx_square_3.Location = new System.Drawing.Point(204, 1);
            this.pbx_square_3.Name = "pbx_square_3";
            this.pbx_square_3.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_3.TabIndex = 2;
            this.pbx_square_3.TabStop = false;
            // 
            // pbx_square_8
            // 
            this.pbx_square_8.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_8.BackgroundImage")));
            this.pbx_square_8.Location = new System.Drawing.Point(712, 1);
            this.pbx_square_8.Name = "pbx_square_8";
            this.pbx_square_8.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_8.TabIndex = 7;
            this.pbx_square_8.TabStop = false;
            // 
            // pbx_square_7
            // 
            this.pbx_square_7.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_7.BackgroundImage")));
            this.pbx_square_7.Location = new System.Drawing.Point(611, 1);
            this.pbx_square_7.Name = "pbx_square_7";
            this.pbx_square_7.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_7.TabIndex = 6;
            this.pbx_square_7.TabStop = false;
            // 
            // pbx_square_6
            // 
            this.pbx_square_6.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_6.BackgroundImage")));
            this.pbx_square_6.Location = new System.Drawing.Point(509, 1);
            this.pbx_square_6.Name = "pbx_square_6";
            this.pbx_square_6.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_6.TabIndex = 5;
            this.pbx_square_6.TabStop = false;
            // 
            // pbx_square_5
            // 
            this.pbx_square_5.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_5.BackgroundImage")));
            this.pbx_square_5.Location = new System.Drawing.Point(407, 1);
            this.pbx_square_5.Name = "pbx_square_5";
            this.pbx_square_5.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_5.TabIndex = 4;
            this.pbx_square_5.TabStop = false;
            // 
            // pbx_square_15
            // 
            this.pbx_square_15.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_15.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_15.BackgroundImage")));
            this.pbx_square_15.Location = new System.Drawing.Point(610, 101);
            this.pbx_square_15.Name = "pbx_square_15";
            this.pbx_square_15.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_15.TabIndex = 15;
            this.pbx_square_15.TabStop = false;
            // 
            // pbx_square_14
            // 
            this.pbx_square_14.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_14.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_14.BackgroundImage")));
            this.pbx_square_14.Location = new System.Drawing.Point(509, 101);
            this.pbx_square_14.Name = "pbx_square_14";
            this.pbx_square_14.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_14.TabIndex = 14;
            this.pbx_square_14.TabStop = false;
            // 
            // pbx_square_13
            // 
            this.pbx_square_13.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_13.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_13.BackgroundImage")));
            this.pbx_square_13.Location = new System.Drawing.Point(407, 101);
            this.pbx_square_13.Name = "pbx_square_13";
            this.pbx_square_13.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_13.TabIndex = 13;
            this.pbx_square_13.TabStop = false;
            // 
            // pbx_square_12
            // 
            this.pbx_square_12.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_12.BackgroundImage")));
            this.pbx_square_12.Location = new System.Drawing.Point(305, 101);
            this.pbx_square_12.Name = "pbx_square_12";
            this.pbx_square_12.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_12.TabIndex = 12;
            this.pbx_square_12.TabStop = false;
            // 
            // pbx_square_11
            // 
            this.pbx_square_11.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_11.BackgroundImage")));
            this.pbx_square_11.Location = new System.Drawing.Point(204, 101);
            this.pbx_square_11.Name = "pbx_square_11";
            this.pbx_square_11.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_11.TabIndex = 11;
            this.pbx_square_11.TabStop = false;
            // 
            // pbx_square_10
            // 
            this.pbx_square_10.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_10.BackgroundImage")));
            this.pbx_square_10.Location = new System.Drawing.Point(102, 101);
            this.pbx_square_10.Name = "pbx_square_10";
            this.pbx_square_10.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_10.TabIndex = 10;
            this.pbx_square_10.TabStop = false;
            // 
            // pbx_square_9
            // 
            this.pbx_square_9.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_9.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_9.BackgroundImage")));
            this.pbx_square_9.Location = new System.Drawing.Point(0, 101);
            this.pbx_square_9.Name = "pbx_square_9";
            this.pbx_square_9.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_9.TabIndex = 9;
            this.pbx_square_9.TabStop = false;
            // 
            // pbx_square_16
            // 
            this.pbx_square_16.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_16.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_16.BackgroundImage")));
            this.pbx_square_16.Location = new System.Drawing.Point(712, 101);
            this.pbx_square_16.Name = "pbx_square_16";
            this.pbx_square_16.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_16.TabIndex = 8;
            this.pbx_square_16.TabStop = false;
            // 
            // pbx_square_24
            // 
            this.pbx_square_24.AccessibleDescription = "";
            this.pbx_square_24.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_24.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_24.BackgroundImage")));
            this.pbx_square_24.Location = new System.Drawing.Point(712, 201);
            this.pbx_square_24.Name = "pbx_square_24";
            this.pbx_square_24.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_24.TabIndex = 23;
            this.pbx_square_24.TabStop = false;
            // 
            // pbx_square_23
            // 
            this.pbx_square_23.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_23.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_23.BackgroundImage")));
            this.pbx_square_23.Location = new System.Drawing.Point(611, 201);
            this.pbx_square_23.Name = "pbx_square_23";
            this.pbx_square_23.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_23.TabIndex = 22;
            this.pbx_square_23.TabStop = false;
            // 
            // pbx_square_22
            // 
            this.pbx_square_22.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_22.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_22.BackgroundImage")));
            this.pbx_square_22.Location = new System.Drawing.Point(509, 201);
            this.pbx_square_22.Name = "pbx_square_22";
            this.pbx_square_22.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_22.TabIndex = 21;
            this.pbx_square_22.TabStop = false;
            // 
            // pbx_square_21
            // 
            this.pbx_square_21.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_21.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_21.BackgroundImage")));
            this.pbx_square_21.Location = new System.Drawing.Point(407, 201);
            this.pbx_square_21.Name = "pbx_square_21";
            this.pbx_square_21.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_21.TabIndex = 20;
            this.pbx_square_21.TabStop = false;
            // 
            // pbx_square_20
            // 
            this.pbx_square_20.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_20.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_20.BackgroundImage")));
            this.pbx_square_20.Location = new System.Drawing.Point(306, 201);
            this.pbx_square_20.Name = "pbx_square_20";
            this.pbx_square_20.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_20.TabIndex = 19;
            this.pbx_square_20.TabStop = false;
            // 
            // pbx_square_19
            // 
            this.pbx_square_19.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_19.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_19.BackgroundImage")));
            this.pbx_square_19.Location = new System.Drawing.Point(204, 201);
            this.pbx_square_19.Name = "pbx_square_19";
            this.pbx_square_19.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_19.TabIndex = 18;
            this.pbx_square_19.TabStop = false;
            // 
            // pbx_square_18
            // 
            this.pbx_square_18.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_18.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_18.BackgroundImage")));
            this.pbx_square_18.Location = new System.Drawing.Point(102, 201);
            this.pbx_square_18.Name = "pbx_square_18";
            this.pbx_square_18.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_18.TabIndex = 17;
            this.pbx_square_18.TabStop = false;
            // 
            // pbx_square_17
            // 
            this.pbx_square_17.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_17.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_17.BackgroundImage")));
            this.pbx_square_17.Location = new System.Drawing.Point(0, 201);
            this.pbx_square_17.Name = "pbx_square_17";
            this.pbx_square_17.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_17.TabIndex = 16;
            this.pbx_square_17.TabStop = false;
            // 
            // pbx_square_40
            // 
            this.pbx_square_40.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_40.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_40.BackgroundImage")));
            this.pbx_square_40.Location = new System.Drawing.Point(712, 401);
            this.pbx_square_40.Name = "pbx_square_40";
            this.pbx_square_40.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_40.TabIndex = 39;
            this.pbx_square_40.TabStop = false;
            // 
            // pbx_square_39
            // 
            this.pbx_square_39.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_39.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_39.BackgroundImage")));
            this.pbx_square_39.Location = new System.Drawing.Point(611, 401);
            this.pbx_square_39.Name = "pbx_square_39";
            this.pbx_square_39.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_39.TabIndex = 38;
            this.pbx_square_39.TabStop = false;
            // 
            // pbx_square_38
            // 
            this.pbx_square_38.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_38.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_38.BackgroundImage")));
            this.pbx_square_38.Location = new System.Drawing.Point(509, 401);
            this.pbx_square_38.Name = "pbx_square_38";
            this.pbx_square_38.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_38.TabIndex = 37;
            this.pbx_square_38.TabStop = false;
            // 
            // pbx_square_37
            // 
            this.pbx_square_37.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_37.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_37.BackgroundImage")));
            this.pbx_square_37.Location = new System.Drawing.Point(407, 401);
            this.pbx_square_37.Name = "pbx_square_37";
            this.pbx_square_37.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_37.TabIndex = 36;
            this.pbx_square_37.TabStop = false;
            // 
            // pbx_square_36
            // 
            this.pbx_square_36.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_36.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_36.BackgroundImage")));
            this.pbx_square_36.Location = new System.Drawing.Point(306, 401);
            this.pbx_square_36.Name = "pbx_square_36";
            this.pbx_square_36.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_36.TabIndex = 35;
            this.pbx_square_36.TabStop = false;
            // 
            // pbx_square_35
            // 
            this.pbx_square_35.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_35.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_35.BackgroundImage")));
            this.pbx_square_35.Location = new System.Drawing.Point(204, 401);
            this.pbx_square_35.Name = "pbx_square_35";
            this.pbx_square_35.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_35.TabIndex = 34;
            this.pbx_square_35.TabStop = false;
            // 
            // pbx_square_34
            // 
            this.pbx_square_34.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_34.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_34.BackgroundImage")));
            this.pbx_square_34.Location = new System.Drawing.Point(102, 401);
            this.pbx_square_34.Name = "pbx_square_34";
            this.pbx_square_34.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_34.TabIndex = 33;
            this.pbx_square_34.TabStop = false;
            // 
            // pbx_square_33
            // 
            this.pbx_square_33.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_33.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_33.BackgroundImage")));
            this.pbx_square_33.Location = new System.Drawing.Point(0, 401);
            this.pbx_square_33.Name = "pbx_square_33";
            this.pbx_square_33.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_33.TabIndex = 32;
            this.pbx_square_33.TabStop = false;
            // 
            // pbx_square_31
            // 
            this.pbx_square_31.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_31.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_31.BackgroundImage")));
            this.pbx_square_31.Location = new System.Drawing.Point(610, 301);
            this.pbx_square_31.Name = "pbx_square_31";
            this.pbx_square_31.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_31.TabIndex = 31;
            this.pbx_square_31.TabStop = false;
            // 
            // pbx_square_30
            // 
            this.pbx_square_30.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_30.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_30.BackgroundImage")));
            this.pbx_square_30.Location = new System.Drawing.Point(509, 301);
            this.pbx_square_30.Name = "pbx_square_30";
            this.pbx_square_30.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_30.TabIndex = 30;
            this.pbx_square_30.TabStop = false;
            // 
            // pbx_square_29
            // 
            this.pbx_square_29.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_29.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_29.BackgroundImage")));
            this.pbx_square_29.Location = new System.Drawing.Point(407, 301);
            this.pbx_square_29.Name = "pbx_square_29";
            this.pbx_square_29.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_29.TabIndex = 29;
            this.pbx_square_29.TabStop = false;
            // 
            // pbx_square_28
            // 
            this.pbx_square_28.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_28.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_28.BackgroundImage")));
            this.pbx_square_28.Location = new System.Drawing.Point(305, 301);
            this.pbx_square_28.Name = "pbx_square_28";
            this.pbx_square_28.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_28.TabIndex = 28;
            this.pbx_square_28.TabStop = false;
            // 
            // pbx_square_27
            // 
            this.pbx_square_27.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_27.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_27.BackgroundImage")));
            this.pbx_square_27.Location = new System.Drawing.Point(204, 301);
            this.pbx_square_27.Name = "pbx_square_27";
            this.pbx_square_27.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_27.TabIndex = 27;
            this.pbx_square_27.TabStop = false;
            // 
            // pbx_square_26
            // 
            this.pbx_square_26.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_26.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_26.BackgroundImage")));
            this.pbx_square_26.Location = new System.Drawing.Point(102, 301);
            this.pbx_square_26.Name = "pbx_square_26";
            this.pbx_square_26.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_26.TabIndex = 26;
            this.pbx_square_26.TabStop = false;
            // 
            // pbx_square_25
            // 
            this.pbx_square_25.AccessibleDescription = "";
            this.pbx_square_25.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_25.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_25.BackgroundImage")));
            this.pbx_square_25.Location = new System.Drawing.Point(0, 301);
            this.pbx_square_25.Name = "pbx_square_25";
            this.pbx_square_25.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_25.TabIndex = 25;
            this.pbx_square_25.TabStop = false;
            // 
            // pbx_square_32
            // 
            this.pbx_square_32.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_32.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_32.BackgroundImage")));
            this.pbx_square_32.Location = new System.Drawing.Point(712, 301);
            this.pbx_square_32.Name = "pbx_square_32";
            this.pbx_square_32.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_32.TabIndex = 24;
            this.pbx_square_32.TabStop = false;
            // 
            // pbx_square_56
            // 
            this.pbx_square_56.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_56.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_56.BackgroundImage")));
            this.pbx_square_56.Location = new System.Drawing.Point(712, 599);
            this.pbx_square_56.Name = "pbx_square_56";
            this.pbx_square_56.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_56.TabIndex = 55;
            this.pbx_square_56.TabStop = false;
            // 
            // pbx_square_55
            // 
            this.pbx_square_55.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_55.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_55.BackgroundImage")));
            this.pbx_square_55.Location = new System.Drawing.Point(611, 599);
            this.pbx_square_55.Name = "pbx_square_55";
            this.pbx_square_55.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_55.TabIndex = 54;
            this.pbx_square_55.TabStop = false;
            // 
            // pbx_square_54
            // 
            this.pbx_square_54.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_54.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_54.BackgroundImage")));
            this.pbx_square_54.Location = new System.Drawing.Point(509, 599);
            this.pbx_square_54.Name = "pbx_square_54";
            this.pbx_square_54.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_54.TabIndex = 53;
            this.pbx_square_54.TabStop = false;
            // 
            // pbx_square_53
            // 
            this.pbx_square_53.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_53.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_53.BackgroundImage")));
            this.pbx_square_53.Location = new System.Drawing.Point(407, 599);
            this.pbx_square_53.Name = "pbx_square_53";
            this.pbx_square_53.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_53.TabIndex = 52;
            this.pbx_square_53.TabStop = false;
            // 
            // pbx_square_52
            // 
            this.pbx_square_52.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_52.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_52.BackgroundImage")));
            this.pbx_square_52.Location = new System.Drawing.Point(306, 599);
            this.pbx_square_52.Name = "pbx_square_52";
            this.pbx_square_52.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_52.TabIndex = 51;
            this.pbx_square_52.TabStop = false;
            // 
            // pbx_square_51
            // 
            this.pbx_square_51.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_51.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_51.BackgroundImage")));
            this.pbx_square_51.Location = new System.Drawing.Point(204, 599);
            this.pbx_square_51.Name = "pbx_square_51";
            this.pbx_square_51.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_51.TabIndex = 50;
            this.pbx_square_51.TabStop = false;
            // 
            // pbx_square_50
            // 
            this.pbx_square_50.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_50.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_50.BackgroundImage")));
            this.pbx_square_50.Location = new System.Drawing.Point(102, 599);
            this.pbx_square_50.Name = "pbx_square_50";
            this.pbx_square_50.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_50.TabIndex = 49;
            this.pbx_square_50.TabStop = false;
            // 
            // pbx_square_49
            // 
            this.pbx_square_49.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_49.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_49.BackgroundImage")));
            this.pbx_square_49.Location = new System.Drawing.Point(0, 599);
            this.pbx_square_49.Name = "pbx_square_49";
            this.pbx_square_49.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_49.TabIndex = 48;
            this.pbx_square_49.TabStop = false;
            // 
            // pbx_square_47
            // 
            this.pbx_square_47.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_47.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_47.BackgroundImage")));
            this.pbx_square_47.Location = new System.Drawing.Point(610, 499);
            this.pbx_square_47.Name = "pbx_square_47";
            this.pbx_square_47.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_47.TabIndex = 47;
            this.pbx_square_47.TabStop = false;
            // 
            // pbx_square_46
            // 
            this.pbx_square_46.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_46.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_46.BackgroundImage")));
            this.pbx_square_46.Location = new System.Drawing.Point(509, 499);
            this.pbx_square_46.Name = "pbx_square_46";
            this.pbx_square_46.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_46.TabIndex = 46;
            this.pbx_square_46.TabStop = false;
            // 
            // pbx_square_45
            // 
            this.pbx_square_45.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_45.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_45.BackgroundImage")));
            this.pbx_square_45.Location = new System.Drawing.Point(407, 499);
            this.pbx_square_45.Name = "pbx_square_45";
            this.pbx_square_45.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_45.TabIndex = 45;
            this.pbx_square_45.TabStop = false;
            // 
            // pbx_square_44
            // 
            this.pbx_square_44.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_44.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_44.BackgroundImage")));
            this.pbx_square_44.Location = new System.Drawing.Point(305, 499);
            this.pbx_square_44.Name = "pbx_square_44";
            this.pbx_square_44.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_44.TabIndex = 44;
            this.pbx_square_44.TabStop = false;
            // 
            // pbx_square_43
            // 
            this.pbx_square_43.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_43.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_43.BackgroundImage")));
            this.pbx_square_43.Location = new System.Drawing.Point(204, 499);
            this.pbx_square_43.Name = "pbx_square_43";
            this.pbx_square_43.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_43.TabIndex = 43;
            this.pbx_square_43.TabStop = false;
            // 
            // pbx_square_42
            // 
            this.pbx_square_42.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_42.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_42.BackgroundImage")));
            this.pbx_square_42.Location = new System.Drawing.Point(102, 499);
            this.pbx_square_42.Name = "pbx_square_42";
            this.pbx_square_42.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_42.TabIndex = 42;
            this.pbx_square_42.TabStop = false;
            // 
            // pbx_square_41
            // 
            this.pbx_square_41.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_41.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_41.BackgroundImage")));
            this.pbx_square_41.Location = new System.Drawing.Point(0, 499);
            this.pbx_square_41.Name = "pbx_square_41";
            this.pbx_square_41.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_41.TabIndex = 41;
            this.pbx_square_41.TabStop = false;
            // 
            // pbx_square_48
            // 
            this.pbx_square_48.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_48.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_48.BackgroundImage")));
            this.pbx_square_48.Location = new System.Drawing.Point(712, 499);
            this.pbx_square_48.Name = "pbx_square_48";
            this.pbx_square_48.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_48.TabIndex = 40;
            this.pbx_square_48.TabStop = false;
            // 
            // pbx_square_63
            // 
            this.pbx_square_63.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_63.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_63.BackgroundImage")));
            this.pbx_square_63.Location = new System.Drawing.Point(610, 697);
            this.pbx_square_63.Name = "pbx_square_63";
            this.pbx_square_63.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_63.TabIndex = 63;
            this.pbx_square_63.TabStop = false;
            // 
            // pbx_square_62
            // 
            this.pbx_square_62.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_62.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_62.BackgroundImage")));
            this.pbx_square_62.Location = new System.Drawing.Point(509, 697);
            this.pbx_square_62.Name = "pbx_square_62";
            this.pbx_square_62.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_62.TabIndex = 62;
            this.pbx_square_62.TabStop = false;
            // 
            // pbx_square_61
            // 
            this.pbx_square_61.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_61.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_61.BackgroundImage")));
            this.pbx_square_61.Location = new System.Drawing.Point(407, 697);
            this.pbx_square_61.Name = "pbx_square_61";
            this.pbx_square_61.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_61.TabIndex = 61;
            this.pbx_square_61.TabStop = false;
            // 
            // pbx_square_60
            // 
            this.pbx_square_60.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_60.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_60.BackgroundImage")));
            this.pbx_square_60.Location = new System.Drawing.Point(305, 697);
            this.pbx_square_60.Name = "pbx_square_60";
            this.pbx_square_60.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_60.TabIndex = 60;
            this.pbx_square_60.TabStop = false;
            // 
            // pbx_square_59
            // 
            this.pbx_square_59.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_59.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_59.BackgroundImage")));
            this.pbx_square_59.Location = new System.Drawing.Point(204, 697);
            this.pbx_square_59.Name = "pbx_square_59";
            this.pbx_square_59.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_59.TabIndex = 59;
            this.pbx_square_59.TabStop = false;
            // 
            // pbx_square_58
            // 
            this.pbx_square_58.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_58.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_58.BackgroundImage")));
            this.pbx_square_58.Location = new System.Drawing.Point(102, 697);
            this.pbx_square_58.Name = "pbx_square_58";
            this.pbx_square_58.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_58.TabIndex = 58;
            this.pbx_square_58.TabStop = false;
            // 
            // pbx_square_57
            // 
            this.pbx_square_57.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_57.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_57.BackgroundImage")));
            this.pbx_square_57.Location = new System.Drawing.Point(0, 697);
            this.pbx_square_57.Name = "pbx_square_57";
            this.pbx_square_57.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_57.TabIndex = 57;
            this.pbx_square_57.TabStop = false;
            // 
            // pbx_square_64
            // 
            this.pbx_square_64.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbx_square_64.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbx_square_64.BackgroundImage")));
            this.pbx_square_64.Location = new System.Drawing.Point(712, 697);
            this.pbx_square_64.Name = "pbx_square_64";
            this.pbx_square_64.Size = new System.Drawing.Size(103, 103);
            this.pbx_square_64.TabIndex = 56;
            this.pbx_square_64.TabStop = false;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(856, 832);
            this.Controls.Add(this.pbx_square_63);
            this.Controls.Add(this.pbx_square_62);
            this.Controls.Add(this.pbx_square_61);
            this.Controls.Add(this.pbx_square_60);
            this.Controls.Add(this.pbx_square_59);
            this.Controls.Add(this.pbx_square_58);
            this.Controls.Add(this.pbx_square_57);
            this.Controls.Add(this.pbx_square_64);
            this.Controls.Add(this.pbx_square_56);
            this.Controls.Add(this.pbx_square_55);
            this.Controls.Add(this.pbx_square_54);
            this.Controls.Add(this.pbx_square_53);
            this.Controls.Add(this.pbx_square_52);
            this.Controls.Add(this.pbx_square_51);
            this.Controls.Add(this.pbx_square_50);
            this.Controls.Add(this.pbx_square_49);
            this.Controls.Add(this.pbx_square_47);
            this.Controls.Add(this.pbx_square_46);
            this.Controls.Add(this.pbx_square_45);
            this.Controls.Add(this.pbx_square_44);
            this.Controls.Add(this.pbx_square_43);
            this.Controls.Add(this.pbx_square_42);
            this.Controls.Add(this.pbx_square_41);
            this.Controls.Add(this.pbx_square_48);
            this.Controls.Add(this.pbx_square_40);
            this.Controls.Add(this.pbx_square_39);
            this.Controls.Add(this.pbx_square_38);
            this.Controls.Add(this.pbx_square_37);
            this.Controls.Add(this.pbx_square_36);
            this.Controls.Add(this.pbx_square_35);
            this.Controls.Add(this.pbx_square_34);
            this.Controls.Add(this.pbx_square_33);
            this.Controls.Add(this.pbx_square_31);
            this.Controls.Add(this.pbx_square_30);
            this.Controls.Add(this.pbx_square_29);
            this.Controls.Add(this.pbx_square_28);
            this.Controls.Add(this.pbx_square_27);
            this.Controls.Add(this.pbx_square_26);
            this.Controls.Add(this.pbx_square_25);
            this.Controls.Add(this.pbx_square_32);
            this.Controls.Add(this.pbx_square_24);
            this.Controls.Add(this.pbx_square_23);
            this.Controls.Add(this.pbx_square_22);
            this.Controls.Add(this.pbx_square_21);
            this.Controls.Add(this.pbx_square_20);
            this.Controls.Add(this.pbx_square_19);
            this.Controls.Add(this.pbx_square_18);
            this.Controls.Add(this.pbx_square_17);
            this.Controls.Add(this.pbx_square_15);
            this.Controls.Add(this.pbx_square_14);
            this.Controls.Add(this.pbx_square_13);
            this.Controls.Add(this.pbx_square_12);
            this.Controls.Add(this.pbx_square_11);
            this.Controls.Add(this.pbx_square_10);
            this.Controls.Add(this.pbx_square_9);
            this.Controls.Add(this.pbx_square_16);
            this.Controls.Add(this.pbx_square_8);
            this.Controls.Add(this.pbx_square_7);
            this.Controls.Add(this.pbx_square_6);
            this.Controls.Add(this.pbx_square_5);
            this.Controls.Add(this.pbx_square_4);
            this.Controls.Add(this.pbx_square_3);
            this.Controls.Add(this.pbx_square_2);
            this.Controls.Add(this.pbx_square_1);
            this.Name = "Main";
            this.Load += new System.EventHandler(this.Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_square_64)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private PictureBox pbx_square_1;
        private PictureBox pbx_square_2;
        private PictureBox pbx_square_4;
        private PictureBox pbx_square_3;
        private PictureBox pbx_square_8;
        private PictureBox pbx_square_7;
        private PictureBox pbx_square_6;
        private PictureBox pbx_square_5;
        private PictureBox pbx_square_15;
        private PictureBox pbx_square_14;
        private PictureBox pbx_square_13;
        private PictureBox pbx_square_12;
        private PictureBox pbx_square_11;
        private PictureBox pbx_square_10;
        private PictureBox pbx_square_9;
        private PictureBox pbx_square_16;
        private PictureBox pbx_square_24;
        private PictureBox pbx_square_23;
        private PictureBox pbx_square_22;
        private PictureBox pbx_square_21;
        private PictureBox pbx_square_20;
        private PictureBox pbx_square_19;
        private PictureBox pbx_square_18;
        private PictureBox pbx_square_17;
        private PictureBox pbx_square_40;
        private PictureBox pbx_square_39;
        private PictureBox pbx_square_38;
        private PictureBox pbx_square_37;
        private PictureBox pbx_square_36;
        private PictureBox pbx_square_35;
        private PictureBox pbx_square_34;
        private PictureBox pbx_square_33;
        private PictureBox pbx_square_31;
        private PictureBox pbx_square_30;
        private PictureBox pbx_square_29;
        private PictureBox pbx_square_28;
        private PictureBox pbx_square_27;
        private PictureBox pbx_square_26;
        private PictureBox pbx_square_25;
        private PictureBox pbx_square_32;
        private PictureBox pbx_square_56;
        private PictureBox pbx_square_55;
        private PictureBox pbx_square_54;
        private PictureBox pbx_square_53;
        private PictureBox pbx_square_52;
        private PictureBox pbx_square_51;
        private PictureBox pbx_square_50;
        private PictureBox pbx_square_49;
        private PictureBox pbx_square_47;
        private PictureBox pbx_square_46;
        private PictureBox pbx_square_45;
        private PictureBox pbx_square_44;
        private PictureBox pbx_square_43;
        private PictureBox pbx_square_42;
        private PictureBox pbx_square_41;
        private PictureBox pbx_square_48;
        private PictureBox pbx_square_63;
        private PictureBox pbx_square_62;
        private PictureBox pbx_square_61;
        private PictureBox pbx_square_60;
        private PictureBox pbx_square_59;
        private PictureBox pbx_square_58;
        private PictureBox pbx_square_57;
        private PictureBox pbx_square_64;
    }
}