﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _13_booleans_und_Vergleichsoperatoren
{
    internal class Program
    {
        static void Main(string[] args)
        {
            bool wahr = true;
            bool falsch = false;

            bool ergebniss = (4 == 5);
            Console.WriteLine(ergebniss);

            ergebniss = (4 != 5);
            Console.WriteLine(ergebniss);

            ergebniss = (4 < 5);
            Console.WriteLine(ergebniss);

            ergebniss = (4 <= 5);
            Console.WriteLine(ergebniss);

            ergebniss = (4 > 5);
            Console.WriteLine(ergebniss);

            ergebniss = (4 >= 5);
            Console.WriteLine(ergebniss);

            Console.ReadKey();
        }
    }
}
