﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_Integer
{
    internal class Program
    {
        static void Main(string[] args)
        {
            byte zahl1 = 13;

            short zahl2 = 22;
            ushort zahl3 = 4;

            int zahl4 = 1337;
            uint zahl5 = 42;

            long zahl6 = 6;
            ulong zahl7 = 7;

            Console.WriteLine(zahl1);
            //overflow
            zahl1 += 255;
            Console.WriteLine(zahl1);
            Console.ReadKey();
        }
    }
}
