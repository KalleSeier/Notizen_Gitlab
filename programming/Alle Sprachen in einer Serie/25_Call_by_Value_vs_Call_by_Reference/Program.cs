﻿namespace _25_Call_by_Value_vs_Refere._2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //ref
            int zahl1 = 69;
            Console.WriteLine(zahl1);
            ZahlÄndern(ref zahl1);
            Console.WriteLine(zahl1);

            //out

            int zahl2;
            ZahlZurückgeben(out zahl2);
            Console.WriteLine(zahl2);

            //listen
            Console.WriteLine("_______________________");

            List<int> list = new List<int>() {13,13,13,13,13,324,235,3465456,4,55,453,234,234,234,56,567,8,67,2453,23434,245,34 };

            ListeVerändern(list);   
            //Liste wird immer mit referenz übergeben.
            foreach(int i in list)
            {
                Console.WriteLine(i);
            }

        }
        public static void ZahlÄndern(ref int zahl)
        {
            zahl = 420;
        }
        public static void ZahlZurückgeben(out int zahl)
        {
            zahl = 1337;
        }
        public static void ListeVerändern(List<int> list)
        {
            for(int i = 0; i < list.Count;i++)
            {
                list[i] = 42;
            }
        }
    }
}