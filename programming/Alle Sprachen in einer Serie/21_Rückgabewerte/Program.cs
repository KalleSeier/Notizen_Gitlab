﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _21_Rückgabewerte
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int zahl = ReturnInt();
            Console.WriteLine(zahl);

            string text = ReturnString();
            Console.WriteLine(text);

            bool wahrheitswert = ReturnBool();
            Console.WriteLine(wahrheitswert);

            double gleitkomma = ReturnDouble();
            Console.WriteLine(gleitkomma);

            Console.ReadKey();
        }
        public static int ReturnInt()
        {
            return 5;
        }
        public static string ReturnString()
        {
            return "Text";
        }
        public static bool ReturnBool()
        {
            return true;
        }
        public static double ReturnDouble()
        {
            return 5.3443;
        }
    }
}
