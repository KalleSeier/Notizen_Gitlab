﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _14_logische_Operatoren
{
    internal class Program
    {
        static void Main(string[] args)
        {
            bool wahr = true;
            bool falsch = false;

            bool ergebniss;

            ergebniss = wahr && falsch;
            Console.WriteLine(ergebniss);

            ergebniss = wahr || falsch;
            Console.WriteLine(ergebniss);

            //Als conditional XOR Ersatz
            ergebniss = wahr =! falsch;
            Console.WriteLine(ergebniss);

            ergebniss = wahr && falsch || wahr && wahr;
            Console.WriteLine(ergebniss);


            Console.ReadKey();
        }
    }
}
