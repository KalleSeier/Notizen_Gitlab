﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _23_Rekursionen
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<int> list = new List<int>();
            for(int i = 0; i < 10000; i++)
            {
                list.Add(i);
            }
            Ausgabe(list);
            Console.ReadKey();
        }
        public static void Ausgabe(List<int> list)
        {
            if(list.Count != 0)
            {
                int element = list[list.Count - 1];
                list.RemoveAt(list.Count - 1);
                Ausgabe(list);
                Console.WriteLine(element);
            }
        }
    }
}
