﻿namespace _23_Rekursionen_2
{
    internal class Program
    {
        static void Main(string[] args)
        {

            Rekursion1();

            Console.ReadKey();
        }
        public static void Rekursion1(int i = 0)
        {
            if (i < 100)
            {
                i++;
                Console.WriteLine(i);
                Rekursion1(i);
            }
        }
        public static void Rekursion2(int i = 0)
        {
            if (i < 100)
            {
                i++;
                Rekursion1(i);
                Console.WriteLine(i);
            }
        }
    }
}