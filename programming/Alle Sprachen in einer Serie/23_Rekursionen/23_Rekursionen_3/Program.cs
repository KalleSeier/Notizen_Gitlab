﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _23_Rekursionen_3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Rekursion();

            Console.ReadKey();
        }
        public static void Rekursion(int zahl1 = 0,int zahl2 = 1)
        {
            if (zahl1 == 0)
            {
                Console.WriteLine(zahl1);
            }
            if((zahl1 + zahl2) < 1000)
            {
                int temp = zahl2;
                zahl2 = zahl2 + zahl1;
                zahl1 = temp;

                Console.WriteLine(zahl2);

                Rekursion(zahl1,zahl2);
            }
        }
    }
}
