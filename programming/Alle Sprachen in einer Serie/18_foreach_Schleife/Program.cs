﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _18_foreach_Schleife
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string name = "Herbert";
            string[] namen = new string[] { "Herbert", "Gerhard", "Siegfried", "Gertrude", "Brigitte" };


            foreach(string a in namen)
            {
                Console.WriteLine(a);
            }

            foreach(char b in name)
            {
                Console.WriteLine(b);
            }

            Console.ReadKey();
        }
    }
}
