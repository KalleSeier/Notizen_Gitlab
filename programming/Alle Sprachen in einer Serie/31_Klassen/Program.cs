﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _31_Klassen
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Schule.SchuleStarten();
            Schüler schüler = new Schüler("Felix");
            schüler.Suizid();
            Console.ReadKey();
        }
    }
    public static class Schule
    {
        public static void SchuleStarten()
        {
            Console.WriteLine("Schule hat gestartet");
        }

    }
    public class Schüler
    {
        string name;
        public Schüler(string name)
        {
            this.name = name;
            Console.WriteLine("{0} wurde erstellt",name);
        }
        public void Suizid()
        {
            Console.WriteLine("{0} hat Suizid begangen",name);
        }
    }


}
