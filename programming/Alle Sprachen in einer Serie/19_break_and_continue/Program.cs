﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _19_break_and_continue
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //break

            while(true)
            {
                Console.WriteLine("in der Schleife");
                break;
            }
            Console.WriteLine("Auserbalb der Schleife");
            Console.WriteLine("___________________________________");

            //continue
            for(int i = 0; i < 1000; i++)
            {
                if(i % 2 == 0)
                {
                    continue;
                }
                //Console.WriteLine("wird nur erreicht wenn es ungerade Zahlen sind");
                Console.WriteLine(i);
            }

            Console.ReadKey();
        }
    }
}
