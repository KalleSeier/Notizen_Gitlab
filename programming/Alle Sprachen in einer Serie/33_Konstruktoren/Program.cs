﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _33_Konstruktoren
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Auto auto = new Auto("Ford Fokus");
            auto.NamenAusgeben();
            Console.ReadKey();
        }
        public class Auto
        {
            string autonamen;
            public Auto(string autonamen)
            {
                this.autonamen = autonamen;
                Console.WriteLine("{0} wurde erstellt", autonamen);
            }
            public void NamenAusgeben()
            {
                Console.WriteLine("Das Auto heißt {0}",autonamen);
            }
        }
    }
}
