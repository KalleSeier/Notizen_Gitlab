﻿namespace _28_Lambda_Funktionen_4
{
    using System.Threading;
    using System.Linq;
    using System.Collections;
    using System.Collections.Generic;
    internal class Program
    {
        public delegate void Test();
        static void Main(string[] args)
        {
            List<string> list = new List<string>() {"Pfirsich","Apfel","Kirsche","Zwetchken","Ananas","Pflaume","Feige","Zitrone","Orange"};

            Func<string, string, string> f1 = (x, y) => x+" "+y;
            Func<int> f2 = () => 5;
            Func<int,int> f3 = x => 5*x;

            Action a = () => Console.WriteLine("Ausgabe");


            Console.WriteLine(f1("Khalil","Seier"));

            foreach (string s in list.Where(x=> x.Substring(0,1) == "A"))
            {
                Console.WriteLine(s);
            }


            Thread thread = new Thread(()=>Ausgabe("Hello World"));
            Test test = new Test(() => Ausgabe("Hello World"));

        }
        public static void Ausgabe(string input)
        { 
            Console.WriteLine(input);
        }
    }
}