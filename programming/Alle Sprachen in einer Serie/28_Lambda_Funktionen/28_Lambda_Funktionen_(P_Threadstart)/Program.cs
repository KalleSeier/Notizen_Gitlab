﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _28_Lambda_Funktionen__P_Threadstart_
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Thread thread = new Thread(() => Ausgabe(13,"Text"));
            thread.Start();
            Console.ReadKey();
        }
        public static void Ausgabe(int zahl,string text)
        {
            Console.WriteLine(zahl);
            Console.WriteLine(text);
        }
    }
}
