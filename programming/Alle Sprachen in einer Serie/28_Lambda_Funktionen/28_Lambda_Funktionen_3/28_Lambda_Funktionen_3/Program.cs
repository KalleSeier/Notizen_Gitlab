﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _28_Lambda_Funktionen_3
{
    class Program
    {
        public delegate void Test();
        static void Main(string[] args)
        {
            List<string> Früchte = new List<string>() { "Äpfel", "Banane", "Pfirsich", "Birne", "Ananas", "Orange" };
            int anzahl = 5;
            string n_frucht = "Apfel/Äpfel";
            Func<string, int, string> f1 = (x, y) => string.Format("{0} {1}", y.ToString(), x);
            Func<int> f2 = () => 2;
            Action<string> ausgabe = (x) => Console.Write(x);
            Action ausgabe2 = () => Console.WriteLine(" sind Toll");
            Console.WriteLine(f1.Invoke(n_frucht,anzahl));
            ausgabe.Invoke(n_frucht);
            ausgabe2.Invoke();
            foreach (var frucht in Früchte.Where(x=> x.Substring(0,1) == "A"))//using System.Linq
            {
                Console.WriteLine(frucht);
            }
            Test test = new Test(() => Ausgabe("a"));
            test();
            test.Invoke();
            Thread t1 = new Thread(() => Ausgabe("a"));
            t1.Start();
            Func<int, int, int> a = (x, y) => x + y;
            Test2(a, 7, 7);
            Console.ReadKey();
        }
        public static void Ausgabe(string a ="")
        {
            Console.WriteLine("Ausgabe");
        }
        public static void Test2(Func<int,int,int> a,int zahl1,int zahl2)
        {
            a.Invoke(zahl1, zahl2);
        }
    }
}
