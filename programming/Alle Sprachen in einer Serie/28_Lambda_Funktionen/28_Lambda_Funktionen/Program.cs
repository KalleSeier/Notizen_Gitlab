﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _28_Lambda_Funktionen
{
    internal class Program
    {
        
        static void Main(string[] args)
        {
            int zahl1 = 7;
            int zahl2 = 3;
            int zahl3;
            int zahl4;
            int zahl5 = 3;

            List<int> list = new List<int>() {10,15,42,1337 };

            foreach( var e in list.Where(x => x%2 == 1) )
            {
                Console.WriteLine(e);
            }
            Console.WriteLine("_________________________________");

            foreach( var e in list.OrderByDescending(x => x))
            {
                Console.WriteLine(e);
            }
            Console.WriteLine("_________________________________");
            //Funktion von Integer nach Integer
            Func<int, int> f1 = x => 2*x;
            zahl3 = f1.Invoke(13);
            Console.WriteLine(zahl3);
            Console.WriteLine("_________________________________");

            zahl4 = Addition((x, y) => x + y, zahl1, zahl2);
            Console.WriteLine(zahl4);
            Console.WriteLine("_________________________________");

            Func<int, int, bool> f2 = (x, y) => 
            {
                return x == y;
            };

            Console.WriteLine(f2.Invoke(zahl1,zahl2));
            Console.WriteLine("_________________________________");

            //Kein Parameter
            Func<char> f3 = () => 'A';
            Console.WriteLine(f3.Invoke());
            Console.WriteLine(f3());
            
            Console.WriteLine("_________________________________");

            Action f4 = () => Console.WriteLine("Text");
            f4();


            Console.WriteLine("____________________________________");
            int numero1 = 9;
            int numero2 = 11;
            Func<int,int,int> f = (x,y) => x*y;
            Action<int, int> a = (x, y) => Console.WriteLine(f.Invoke(x, y));
            a.Invoke(numero1,numero2);



            Console.ReadKey();
        }
        public static int Addition(Func<int,int,int> function,int zahl1, int zahl2)
        {
            return (function.Invoke(zahl1, zahl2));
        }

    }
}
