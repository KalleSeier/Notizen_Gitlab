﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _28_lambda_Funktionen_2
{
    internal class Program
    {
        public delegate int HochSichSelbst();
        static void Main(string[] args)
        {
            int zahl1 = 12;
            HochSichSelbst hoch = new HochSichSelbst( () => Hoch(zahl1));
            int zahl2 = hoch.Invoke();
            Console.WriteLine(zahl2);

            Console.ReadLine();
        }
        public static int Hoch(int zahl)
        {
            return zahl * zahl;
        }
    }
}
