﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _39_Methoden_Überschreiben_3
{
    internal class Weinglas : Behälter
    {
        public override void Beschreibung()
        {
            Console.WriteLine("Ich bin ein Weinglas");
        }
    }
}
