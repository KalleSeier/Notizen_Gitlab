﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _39_Methoden_Überschreiben_3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Behälter behälter = new Behälter();
            Weinglas weinglas = new Weinglas(); 
            Becher becher = new Becher();

            behälter.Beschreibung();
            weinglas.Beschreibung();
            becher.Beschreibung();
            Console.WriteLine();

            List<Behälter> behälters = new List<Behälter>() { behälter, weinglas, becher };
            
            foreach(Behälter behälter1 in behälters)
            {
                behälter1.Beschreibung();
            }

            Console.ReadKey();
        }
    }
}
