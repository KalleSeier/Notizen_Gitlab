﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _39_Methoden_überschreiben
{
    internal class Flugzeug : Fortbewegungsmittel
    {
        public override void Fortbewegen()
        {
            Console.WriteLine("Das Flugzeug fliegt");
        }
    }
}
