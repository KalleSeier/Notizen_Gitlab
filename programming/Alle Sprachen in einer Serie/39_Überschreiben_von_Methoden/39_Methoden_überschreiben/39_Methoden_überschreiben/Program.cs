﻿namespace _39_Methoden_überschreiben
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Fortbewegungsmittel fortbewegungsmittel = new Fortbewegungsmittel();

            Auto auto = new Auto();
            Flugzeug flugzeug = new Flugzeug();
            Fahrrad fahrrad = new Fahrrad();

            fortbewegungsmittel.Fortbewegen();
            auto.Fortbewegen();
            fahrrad.Fortbewegen();
            flugzeug.Fortbewegen();

            Console.WriteLine("\n\n");

            //Dynamische Bindung

            List<Fortbewegungsmittel> list = new List<Fortbewegungsmittel>() {fortbewegungsmittel,auto,fahrrad,flugzeug };
            
            foreach(Fortbewegungsmittel fortbewegungsmittel1 in list)
            {
                fortbewegungsmittel1.Fortbewegen();
            }

            pretend_im_a_List2cs pretend = new pretend_im_a_List2cs();

            pretend.Add();

        }
    }
}