﻿namespace _27_Function_Pointer_4
{
    internal class Program
    {
        public delegate string Test();
        static void Main(string[] args)
        {
            Func<string> f1;
            f1 = ReturnValue;
            Console.WriteLine(f1());

            Test test = ReturnValue;
            test += ReturnValue;
            Console.WriteLine(test());

        }
        public static string ReturnValue()
        {
            return "Value";
        }
    }
}