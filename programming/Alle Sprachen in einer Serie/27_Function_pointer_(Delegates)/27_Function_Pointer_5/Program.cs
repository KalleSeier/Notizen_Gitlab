﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _27_Function_Pointer_5
{
    internal class Program
    {
        public delegate void Test();
        static void Main(string[] args)
        {
            Func<int, int, int> f1 = (x, y) => x + y;
            Console.WriteLine(f1(13,12));

            Test test = new Test(Ausgabe);
            Test test2 = new Test(() => Rechnen(14,13));

            f1.Invoke(13,13);
            test2();
            test();

            Console.ReadKey();
        }
        public static int Rechnen(int zahl1,int zahl2)
        {
            Console.WriteLine(zahl1 + zahl2);
            return zahl1 + zahl2;
        }
        public static void Ausgabe()
        {
            Console.WriteLine("Ausgabe");
        }
    }
}
