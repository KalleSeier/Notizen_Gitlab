﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _27_Function_Pointer__Delegate_
{
    internal class Program
    {
        public delegate int Rechenoperation(int zahl1, int zahl2);
        static void Main(string[] args)
        {
            // Ein delegate ist ein Methodenzeiger
            bool ueberpruefung_rechenoperation = false;
            Rechenoperation rechenoperation = new Rechenoperation(Error);
            bool ueberpruefung_zahl1 = false;
            int zahl1;
            int zahl2;
            int ergebniss;
            while (!ueberpruefung_rechenoperation)
            {
                ueberpruefung_rechenoperation = true;

                Console.WriteLine("wollen sie plus oder minus-Rechnen? Geben sie 'p' für plus oder 'm' für minus ein");
                string eingabe = Console.ReadLine();
                if (eingabe == "p")
                {
                    rechenoperation = new Rechenoperation(Addition);
                }
                else if (eingabe == "m")
                {
                    rechenoperation = new Rechenoperation(Substratkion);
                }
                else
                {
                    ueberpruefung_rechenoperation=false;
                    Console.Clear();
                    Console.WriteLine("Error");
                }
            }
            zahl1 = ZahlAuslesen(ueberpruefung_zahl1, "Geben Sie die erste Zahl ein");
            zahl2 = ZahlAuslesen(ueberpruefung_zahl1, "Geben Sie die zweite Zahl ein");

            ergebniss = rechenoperation.Invoke(zahl1, zahl2);

            Console.WriteLine("Ihr Ergebniss ist {0}", ergebniss);

            Console.ReadKey();

        }
        public static int Addition(int zahl1, int zahl2)
        {
            return zahl1 + zahl2;
        }
        public static int Substratkion(int zahl1,int zahl2)
        {
            return zahl1 - zahl2;
        }
        public static int Error(int zahl1,int zahl2)
        {
            Console.WriteLine("Es ist ein Fehler aufgetreten");
            return -10;
        }

        public static int ZahlAuslesen(bool ueberpruefung_zahl,string text)
        {
            while (!ueberpruefung_zahl)
            {
                    Console.WriteLine(text);
                    if (int.TryParse(Console.ReadLine(), out int cache))
                    {
                        return cache;
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("Error");
                    }
                
            }
            return -10;
        }
    }
}
