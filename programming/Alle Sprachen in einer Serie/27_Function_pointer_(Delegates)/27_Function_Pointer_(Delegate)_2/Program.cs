﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _27_Function_Pointer__Delegate__2
{
    internal class Program
    {
        public delegate void Ausgabe(List<int> list);
        static void Main(string[] args)
        {
            List<int> list = new List<int>();

            for(int i = 0; i < 10000;i++)
            {
                list.Add(i);
            }

            Ausgabe ausgabe = new Ausgabe(Zweierreihe);
            ausgabe += UngeradeZahlen;

            ausgabe.Invoke(list);

            Console.ReadKey();

        }
        public static void Zweierreihe(List<int> list)
        {
            Console.WriteLine("Zweierreihe");
            foreach(int l in list)
            {
                if(l % 2 == 0)
                {
                    Console.WriteLine(l);
                }
            }
            Console.WriteLine("__________________________________________-----");
        }
        public static void UngeradeZahlen(List<int> list)
        {
            Console.WriteLine("UngeradeZahlen");
            foreach (int l in list)
            {
                if (l % 2 != 0)
                {
                    Console.WriteLine(l);
                }
            }
            Console.WriteLine("__________________________________________-----");
        }
    }
}
