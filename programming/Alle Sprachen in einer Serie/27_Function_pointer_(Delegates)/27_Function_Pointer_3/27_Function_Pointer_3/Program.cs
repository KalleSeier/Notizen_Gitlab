﻿namespace _27_Function_Pointer_3
{
    internal class Program
    {
        public delegate void DelTest();
        public delegate int DelTes2(int zahl, string eingabe);
        static void Main(string[] args)
        {
            Func<string> f1;
            Func<int,string,int> f2;
            Action<int> a1;
            Action a2;

            DelTest d1 = new DelTest(Ausgabe2);
            DelTes2 d2 = new DelTes2(Berrechnung);

            f1 = Ausgabe;
            f2 = Berrechnung;
            a2 = Ausgabe3;
            a1 = Ausgabe4;


            d1.Invoke();
            d2(7,"4");

            f1();
            f2.Invoke(5, "4");

            a2.Invoke();
            a1(7);

            Console.ReadKey();
        }
        public static string Ausgabe()
        {
            Console.WriteLine("Hello world");
            return "Hello World";
        }
        public static int Berrechnung(int zahl2, string eingabe)
        {
            if(int.TryParse(eingabe, out int zahl))
            {
                Console.WriteLine(zahl*zahl2);
            }
            else
            {
                Console.WriteLine("Die Eingabe ist keien Zahl");
            }
            return zahl2 * zahl;
        }
        public static void Ausgabe2()
        {
            Console.WriteLine("Hello world");
        }
        public static void Ausgabe3()
        {
            Console.WriteLine("Hello World");
        }
        public static void Ausgabe4(int zahl)
        {
            Console.WriteLine(zahl);
        }
    }
}