﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _8_Mehrdimensionale_Arrays
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int[,] array1 = new int[3,2];
            int[,] array = new int[,] { { 1, 2 }, { 3, 4 }, { 5, 10 } };
            array1 = array;
            //Die Länge an der ersten Stelle
            Console.WriteLine(array1.GetLength(0));
            //Die Länge an der zweiten Stelle
            Console.WriteLine(array1.GetLength(1));
            foreach (int i in array1)
            {
                Console.WriteLine(i);
            }
            Console.ReadLine();
        }
    }
}
