﻿using System;

namespace _24_Default_Argumente_2
{
    class Program
    {
        static void Main(string[] args)
        {
            string name = "Khalil";
            Begrüßung();
            Begrüßung(name);
            Console.ReadKey();
        }
        public static void Begrüßung(string name="Benutzer")
        {
            Console.WriteLine("Hallo {0}",name)a;
        }
    }
}
