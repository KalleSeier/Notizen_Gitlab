﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _24_Default_Argumente
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Ausgabe();
            Console.ReadKey();
        }
        public static void Ausgabe(string Text = "Hänsel und Gretel verliefen sich in einem Wald")
        {
            Console.WriteLine(Text);
        }
    }
}
