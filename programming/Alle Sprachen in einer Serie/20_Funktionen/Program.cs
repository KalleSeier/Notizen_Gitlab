﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20_Funktionen
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Schreiben();
            Console.ReadKey();
        }
        public static void Schreiben()
        {
            for(int i = 0; i < 10; i++)
            {
                Console.WriteLine("{0} Das ist eine Funktion",i);
            }
        }
    }
}
