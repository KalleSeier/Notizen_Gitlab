﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _22_Parameter
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string hänsel = "Hänsel und Gretel verliefen sich im Wald";
            Function(hänsel);
            Console.ReadKey();
        }
        public static void Function(string Text)
        {
            Console.WriteLine(Text);
        }
    }
}
