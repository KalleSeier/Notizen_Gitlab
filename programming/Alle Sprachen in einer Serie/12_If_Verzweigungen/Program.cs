﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _12_If_Verzweigungen
{
    internal class Program
    {
        static void Main(string[] args)
        {
            if( 1 == 1)
            {
                Console.WriteLine("Du bist ein Mathe Genie");

            }

            if ( 15 > 16 )
            {
                Console.WriteLine("Howdy");
            }
            else if(15 < 16)
            {
                Console.WriteLine("true that");
            }
            
            if( "a" == "b")
            {
                Console.WriteLine("not True");
            }
            else
            {
                Console.WriteLine("True");
            }

            Console.ReadKey();
        }
    }
}
