﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _15_Call_by_Value_vs_Call_by_Reference
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int zahl1 = 5;
            Zahländern(ref zahl1);
            Console.WriteLine(zahl1);

            Console.WriteLine("___________________________");
            Zahländern2(out int zahl2);
            Console.WriteLine(zahl2);

            //Listen werden generell mit Call by reference übergeben
            List<string> list = new List<string> { "der", "King", "ist", "back" };
            ListeÄndern(list);
            foreach (string s in list)
            {
                Console.WriteLine(s);
            }
            Console.WriteLine("____________________________");


            Console.ReadKey();

        }
        public static void Zahländern(ref int zahl1)
        {
            zahl1 = 10;
        }
        public static void Zahländern2(out int zahl2)
        {
            zahl2 = 17;
        }
        public static void ListeÄndern(List<string> list)
        {
            for(int i = 0; i < list.Count; i++)
            {
                list[i] = "a";
            }
        }
    }
}
