﻿namespace _34_Zugriffsmodifikatoren_2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Test test = new Test();
            test.Schuljahr = 5;
            // test.alter = 3; 'Der Zugriff auf "Program.Test.alter" ist aufgrund des Schutzgrads nicht möglich.
            test.Spitzname = "Mooncake";
            Console.WriteLine(test.Spitzname);

            Console.ReadKey();

        }
        class Test
        {
            private int alter;
            public int Schuljahr;

            public int Alter { 
                get
                {
                    return alter;
                }
                set 
                {
                    //alter = value; 
                }
            }

            public string Spitzname { get; set; }
        }
    }
}