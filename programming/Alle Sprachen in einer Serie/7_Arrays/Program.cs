﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7_Arrays
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int[] array = new int[5];
            int[] array2 = new int[] { 1337,42,13,33,1 };
            array = array2;
            for(int i = 0; i < array.Length; i++)
            {
                Console.WriteLine(i);
            }
            Console.ReadKey();
        }
    }
}
