﻿namespace _37_Typecasts_2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<Schule> schuleList = new List<Schule>() { new Schule(), new Schule(),new Gymnasium(), new Handelsakademi(), new Handelsakademi(), new Gymnasium(), new Schule(), new Schule(), new Gymnasium(), new Handelsakademi(), new Handelsakademi(), new Gymnasium() };
            List<Gymnasium> gymnasia = new List<Gymnasium>() { new Gymnasium(), new Gymnasium(), new Gymnasium(), new Gymnasium(), new Gymnasium(), new Gymnasium(), new Gymnasium(), new Gymnasium(), new Gymnasium(), new Gymnasium(), new Gymnasium(), new Gymnasium(), new Gymnasium() };

            //Downcast
            foreach (Schule schule in schuleList)
            {
                if(schule.GetType() == typeof(Handelsakademi))
                {
                    Handelsakademi handelsakademi = (Handelsakademi)schule;//hard Cast. will throw exception if i doesnt work
                    handelsakademi.AusgabeHAK();
                }
            }
            //Upcast
            foreach(Gymnasium gymnasium in gymnasia)
            {
                Schule schule = gymnasium as Schule;//Soft Cast. schule will be null if it doesnt work
                if (schule != null)                 //soft cast
                {
                    schule.AusgabeSchule();
                }
            }
        
            
        
        }
    }
}