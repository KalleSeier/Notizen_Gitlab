﻿namespace _37_Typecasts
{
    internal class Program
    {
        static void Main(string[] args)
        {
            double zahl1 = 3.5;
            int zahl2 = (int)zahl1;

            HTL_er htler = new HTL_er("Gerhard",23);
            Schüler schüler = new Schüler("Siegfried",56);
         
            //Upcast
            Schüler sch = htler;
            sch.Besaufen();

            //Downcast 
            //HTL_er htler2 = (HTL_er)schüler; // führt zu fehlern
            HTL_er htler2 = (HTL_er)sch;
            htler.suizid();

            Console.WriteLine(zahl2);


            HTL_er hTL_Er = new HTL_er("Robert", 20);
            double zahl3 = 7.3;

            //Hard Cast

            Schüler schüler1 = (HTL_er)hTL_Er;
            schüler1.Besaufen();
            int zahl4 = (int)zahl3;
            Console.WriteLine(zahl4);

            //Soft Cast

            Schüler schüler2 = hTL_Er as Schüler;
            if(schüler2 != null)
            {
                schüler2.Besaufen();
            }
            int zahl5 = Convert.ToInt32(zahl4);
            Console.WriteLine(zahl5);


            Console.ReadKey();
        }
    }
}