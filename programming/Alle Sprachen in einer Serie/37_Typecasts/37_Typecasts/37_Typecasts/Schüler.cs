﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _37_Typecasts
{
    internal class Schüler
    {
        protected string name;
        int alter;
        public Schüler(string name, int alter)
        {
            this.name = name;
            this.alter = alter;
        }
        public void Besaufen()
        {
            Console.WriteLine("Der Schüler {0} besauft sich",name);
        }
    }
}
