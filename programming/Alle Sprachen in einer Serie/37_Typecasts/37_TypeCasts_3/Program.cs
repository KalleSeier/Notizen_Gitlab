﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _37_TypeCasts_3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<Pokemon> pokemons = new List<Pokemon>() { new Pokemon(), new Pokemon(), new Wasserpokemon(), new Feuerpokemon(), new Wasserpokemon(), new Feuerpokemon() };


            foreach (Pokemon pokemon in pokemons)
            {
                if (pokemon.GetType() == typeof(Wasserpokemon))
                {
                    Wasserpokemon wasserpokemon = pokemon as Wasserpokemon; // Soft Cast
                    if (wasserpokemon != null)
                    {
                        wasserpokemon.Beschreibung();
                        wasserpokemon.Attacke();
                    }

                }
                if (pokemon.GetType() == typeof(Feuerpokemon))
                {
                    Feuerpokemon feuerpokemon = (Feuerpokemon)pokemon;// Hard Cast

                    feuerpokemon.Beschreibung();
                    feuerpokemon.Attacke();
                }
            }

            Console.ReadKey();
        }
    }
}
