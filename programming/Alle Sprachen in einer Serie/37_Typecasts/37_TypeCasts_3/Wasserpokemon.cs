﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _37_TypeCasts_3
{
    internal class Wasserpokemon : Pokemon
    {
        public void Attacke()
        {
            Console.WriteLine("Platsch");
        }
        public override void Beschreibung()
        {
            Console.WriteLine("Ich bin ein Wasserpokemon");
        }
    }
}
