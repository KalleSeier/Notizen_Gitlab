﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _37_TypeCasts_3
{
    internal class Feuerpokemon : Pokemon
    {
        public override void Beschreibung()
        {
            Console.WriteLine("Ich bin ein Feuerpokemon");
        }
        public void Attacke()
        {
            Console.WriteLine("Brenne");
        }
    }
}
