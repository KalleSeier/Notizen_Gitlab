﻿
namespace aimbotwithkalle
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lbl_Player_x = new System.Windows.Forms.Label();
            this.lbl_Player_y = new System.Windows.Forms.Label();
            this.lbl_Player_z = new System.Windows.Forms.Label();
            this.lbl_Player_Health = new System.Windows.Forms.Label();
            this.cb_Player_Health = new System.Windows.Forms.CheckBox();
            this.tbx_Player_Health = new System.Windows.Forms.TextBox();
            this.lbl_Player_set_Health = new System.Windows.Forms.Label();
            this.tbx_set_Player_Health = new System.Windows.Forms.TextBox();
            this.cb_Rage_hack = new System.Windows.Forms.CheckBox();
            this.cb_infinite_Ammo = new System.Windows.Forms.CheckBox();
            this.lbl_Enemy_Health = new System.Windows.Forms.Label();
            this.tbx_Enemy_HP = new System.Windows.Forms.TextBox();
            this.tbx_closest_Enemy = new System.Windows.Forms.TextBox();
            this.lbl_closest_enemy = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(13, 13);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(119, 13);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(110, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Start Application";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lbl_Player_x
            // 
            this.lbl_Player_x.AutoSize = true;
            this.lbl_Player_x.Location = new System.Drawing.Point(13, 40);
            this.lbl_Player_x.Name = "lbl_Player_x";
            this.lbl_Player_x.Size = new System.Drawing.Size(12, 13);
            this.lbl_Player_x.TabIndex = 2;
            this.lbl_Player_x.Text = "x";
            // 
            // lbl_Player_y
            // 
            this.lbl_Player_y.AutoSize = true;
            this.lbl_Player_y.Location = new System.Drawing.Point(13, 57);
            this.lbl_Player_y.Name = "lbl_Player_y";
            this.lbl_Player_y.Size = new System.Drawing.Size(12, 13);
            this.lbl_Player_y.TabIndex = 3;
            this.lbl_Player_y.Text = "y";
            // 
            // lbl_Player_z
            // 
            this.lbl_Player_z.AutoSize = true;
            this.lbl_Player_z.Location = new System.Drawing.Point(13, 74);
            this.lbl_Player_z.Name = "lbl_Player_z";
            this.lbl_Player_z.Size = new System.Drawing.Size(12, 13);
            this.lbl_Player_z.TabIndex = 4;
            this.lbl_Player_z.Text = "z";
            // 
            // lbl_Player_Health
            // 
            this.lbl_Player_Health.AutoSize = true;
            this.lbl_Player_Health.Location = new System.Drawing.Point(10, 108);
            this.lbl_Player_Health.Name = "lbl_Player_Health";
            this.lbl_Player_Health.Size = new System.Drawing.Size(73, 13);
            this.lbl_Player_Health.TabIndex = 5;
            this.lbl_Player_Health.Text = "Player Health:";
            // 
            // cb_Player_Health
            // 
            this.cb_Player_Health.AutoSize = true;
            this.cb_Player_Health.Location = new System.Drawing.Point(195, 107);
            this.cb_Player_Health.Name = "cb_Player_Health";
            this.cb_Player_Health.Size = new System.Drawing.Size(92, 17);
            this.cb_Player_Health.TabIndex = 6;
            this.cb_Player_Health.Text = "Freeze Health";
            this.cb_Player_Health.UseVisualStyleBackColor = true;
            // 
            // tbx_Player_Health
            // 
            this.tbx_Player_Health.Location = new System.Drawing.Point(89, 105);
            this.tbx_Player_Health.Name = "tbx_Player_Health";
            this.tbx_Player_Health.ReadOnly = true;
            this.tbx_Player_Health.Size = new System.Drawing.Size(100, 20);
            this.tbx_Player_Health.TabIndex = 7;
            // 
            // lbl_Player_set_Health
            // 
            this.lbl_Player_set_Health.AutoSize = true;
            this.lbl_Player_set_Health.Location = new System.Drawing.Point(293, 108);
            this.lbl_Player_set_Health.Name = "lbl_Player_set_Health";
            this.lbl_Player_set_Health.Size = new System.Drawing.Size(73, 13);
            this.lbl_Player_set_Health.TabIndex = 8;
            this.lbl_Player_set_Health.Text = "Player Health:";
            // 
            // tbx_set_Player_Health
            // 
            this.tbx_set_Player_Health.Location = new System.Drawing.Point(372, 104);
            this.tbx_set_Player_Health.Name = "tbx_set_Player_Health";
            this.tbx_set_Player_Health.Size = new System.Drawing.Size(100, 20);
            this.tbx_set_Player_Health.TabIndex = 9;
            this.tbx_set_Player_Health.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbx_set_Player_Health_KeyUp);
            // 
            // cb_Rage_hack
            // 
            this.cb_Rage_hack.AutoSize = true;
            this.cb_Rage_hack.Location = new System.Drawing.Point(13, 187);
            this.cb_Rage_hack.Name = "cb_Rage_hack";
            this.cb_Rage_hack.Size = new System.Drawing.Size(76, 17);
            this.cb_Rage_hack.TabIndex = 10;
            this.cb_Rage_hack.Text = "Ragehack";
            this.cb_Rage_hack.UseVisualStyleBackColor = true;
            // 
            // cb_infinite_Ammo
            // 
            this.cb_infinite_Ammo.AutoSize = true;
            this.cb_infinite_Ammo.Location = new System.Drawing.Point(13, 154);
            this.cb_infinite_Ammo.Name = "cb_infinite_Ammo";
            this.cb_infinite_Ammo.Size = new System.Drawing.Size(88, 17);
            this.cb_infinite_Ammo.TabIndex = 11;
            this.cb_infinite_Ammo.Text = "infinite Ammo";
            this.cb_infinite_Ammo.UseVisualStyleBackColor = true;
            // 
            // lbl_Enemy_Health
            // 
            this.lbl_Enemy_Health.AutoSize = true;
            this.lbl_Enemy_Health.Location = new System.Drawing.Point(14, 259);
            this.lbl_Enemy_Health.Name = "lbl_Enemy_Health";
            this.lbl_Enemy_Health.Size = new System.Drawing.Size(45, 13);
            this.lbl_Enemy_Health.TabIndex = 12;
            this.lbl_Enemy_Health.Text = "Enemy1";
            // 
            // tbx_Enemy_HP
            // 
            this.tbx_Enemy_HP.Location = new System.Drawing.Point(65, 256);
            this.tbx_Enemy_HP.Name = "tbx_Enemy_HP";
            this.tbx_Enemy_HP.Size = new System.Drawing.Size(100, 20);
            this.tbx_Enemy_HP.TabIndex = 13;
            // 
            // tbx_closest_Enemy
            // 
            this.tbx_closest_Enemy.Location = new System.Drawing.Point(140, 296);
            this.tbx_closest_Enemy.Name = "tbx_closest_Enemy";
            this.tbx_closest_Enemy.ReadOnly = true;
            this.tbx_closest_Enemy.Size = new System.Drawing.Size(100, 20);
            this.tbx_closest_Enemy.TabIndex = 14;
            // 
            // lbl_closest_enemy
            // 
            this.lbl_closest_enemy.AutoSize = true;
            this.lbl_closest_enemy.Location = new System.Drawing.Point(10, 299);
            this.lbl_closest_enemy.Name = "lbl_closest_enemy";
            this.lbl_closest_enemy.Size = new System.Drawing.Size(124, 13);
            this.lbl_closest_enemy.TabIndex = 15;
            this.lbl_closest_enemy.Text = "Closest Enemy Distance:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(911, 544);
            this.Controls.Add(this.lbl_closest_enemy);
            this.Controls.Add(this.tbx_closest_Enemy);
            this.Controls.Add(this.tbx_Enemy_HP);
            this.Controls.Add(this.lbl_Enemy_Health);
            this.Controls.Add(this.cb_infinite_Ammo);
            this.Controls.Add(this.cb_Rage_hack);
            this.Controls.Add(this.tbx_set_Player_Health);
            this.Controls.Add(this.lbl_Player_set_Health);
            this.Controls.Add(this.tbx_Player_Health);
            this.Controls.Add(this.cb_Player_Health);
            this.Controls.Add(this.lbl_Player_Health);
            this.Controls.Add(this.lbl_Player_z);
            this.Controls.Add(this.lbl_Player_y);
            this.Controls.Add(this.lbl_Player_x);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lbl_Player_x;
        private System.Windows.Forms.Label lbl_Player_y;
        private System.Windows.Forms.Label lbl_Player_z;
        private System.Windows.Forms.Label lbl_Player_Health;
        private System.Windows.Forms.CheckBox cb_Player_Health;
        private System.Windows.Forms.TextBox tbx_Player_Health;
        private System.Windows.Forms.Label lbl_Player_set_Health;
        private System.Windows.Forms.TextBox tbx_set_Player_Health;
        private System.Windows.Forms.CheckBox cb_Rage_hack;
        private System.Windows.Forms.CheckBox cb_infinite_Ammo;
        private System.Windows.Forms.Label lbl_Enemy_Health;
        private System.Windows.Forms.TextBox tbx_Enemy_HP;
        private System.Windows.Forms.TextBox tbx_closest_Enemy;
        private System.Windows.Forms.Label lbl_closest_enemy;
    }
}

