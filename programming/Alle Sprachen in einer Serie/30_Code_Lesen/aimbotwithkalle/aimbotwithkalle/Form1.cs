﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Memory;
using System.Threading;
namespace aimbotwithkalle
{
    public partial class Form1 : Form
    {
        public static string ExecName = "ac_client"; //des muss ohne .exe sein
        public static string ProcessName = "ac_client.exe";
        public string Base = "";
        public int Number_of_Enemys = 0;
        float playerx, playery, playerz;
        int number_of_closest_Enemy = 0;
        CheatFunctions CF = new CheatFunctions(ExecName, ProcessName);
        public Form1()
        {
            InitializeComponent();
            
        }

        void Aimbot()
        {
            if (Number_of_Enemys == 0)
            {
                textBox1.Text = Convert.ToString(CF.Enemy_Get_Count2());
                Number_of_Enemys = CF.Enemy_Get_Count2();
            }
            playerx = CF.Player_Get_X();
            playery = CF.Player_Get_Y();
            playerz = CF.Player_Get_Z();
            //CF.Enemy_Get_Closest_Enemy(Number_of_Enemys,playerx,playery,playerz); //hier muss noch meine x und meine y mitgegeben werden, damit ich die relative länge berechnen kann.
            //hier dann auf den Closest Enemy schauen (umwandlung von x-y-z auf yawn pitch etc.
            tbx_closest_Enemy.Text = Convert.ToString(CF.Enemy_Get_Closest_Enemy_Alive(Number_of_Enemys, playerx, playery, playerz,ref number_of_closest_Enemy));
           // textBox1.Text = Convert.ToString(number_of_closest_Enemy);
            CF.Player_Aim_at_Enemy(number_of_closest_Enemy,playerx,playery,playerz);
        }
        
        private void update_coordinates()
        {
            lbl_Player_x.Text = Convert.ToString(CF.Player_Get_X());
            lbl_Player_y.Text = Convert.ToString(CF.Player_Get_Y());
            lbl_Player_z.Text = Convert.ToString(CF.Player_Get_Z());
        }

        private void button1_Click(object sender, EventArgs e) //debug purpose only
        {
            textBox1.Text = Convert.ToString(CF.IsGameRunning());
            textBox1.Text = Convert.ToString(CF.GetBaseAddress(ProcessName));
            textBox1.Text = Convert.ToString(CF.Player_Get_Health());
        }

        private void update()
        {
            update_coordinates();
            if (cb_Player_Health.Checked)
            {
                CF.Player_Health_Freeze(Convert.ToInt32(tbx_Player_Health.Text));
            }
            update_health();
            if (cb_infinite_Ammo.Checked) 
            {
                CF.Player_Set_Ammunition();
            }
            if (cb_Rage_hack.Checked)
            {
                Aimbot();
            }
            Enemy_Health();

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            update();
        }
        private void update_health()
        {
            tbx_Player_Health.Text = CF.Player_Get_Health().ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void tbx_set_Player_Health_KeyUp(object sender, KeyEventArgs e)
        {
            CF.Player_Set_Health(Convert.ToInt32(tbx_set_Player_Health.Text));
        }

        private void Enemy_Health()
        {
            tbx_Enemy_HP.Text = Convert.ToString(CF.Enemy_Get_Health());
        }

    }
}
