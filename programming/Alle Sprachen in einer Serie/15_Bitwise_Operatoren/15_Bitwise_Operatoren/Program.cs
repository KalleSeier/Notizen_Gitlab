﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _15_Bitwise_Operatoren
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Bitwise Und (&)

            int number1 = 23;
            int number2 = 14;

            int number3 = number1 & number2;
            Console.WriteLine("10111");
            Console.WriteLine("01110");
            Console.WriteLine("00110");
            Console.WriteLine(number3);
            Console.WriteLine("______________________________________");

            //Bitwise Oder (|)

            int número1 = 23;
            int número2 = 14;

            int número3 = número1 | número2;
            Console.WriteLine("10111");
            Console.WriteLine("01110");
            Console.WriteLine("11111");
            Console.WriteLine(número3);
            Console.WriteLine("______________________________________");

            //Bitwise EntwederOder ^

            int numero1 = 23;
            int numero2 = 14;

            int numero3 = numero1 ^ numero2;
            Console.WriteLine("10111");
            Console.WriteLine("01110");
            Console.WriteLine("10001");
            Console.WriteLine(numero3);
            Console.WriteLine("______________________________________");

            //Bitshiften

            int zahl = 2;
            Console.WriteLine(zahl);

            int zahl2 = zahl << 2;
            Console.WriteLine(zahl2);

            zahl2 = zahl <<10;
            Console.WriteLine(zahl2);

            int zahl3 = zahl2 >>10;
            Console.WriteLine(zahl3);


            Console.ReadKey();
        }
    }
}
