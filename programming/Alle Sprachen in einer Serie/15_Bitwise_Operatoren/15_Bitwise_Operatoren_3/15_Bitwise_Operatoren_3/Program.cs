﻿using System;

namespace _15_Bitwise_Operatoren_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("9 UND 5 {0}",(9&5));
            Console.WriteLine("9 ODER 5 {0}",(9|5));
            Console.WriteLine("13 ODER 7 {0}",(9^5));

            Console.WriteLine("2 << 3: {0}",2<<3);
            Console.WriteLine("64 >> 2: {0}",64>>2);

            //Ergiebt alles keinen Sinn
            Console.WriteLine("13 + komplement von 5: {0}",13+~5);
            Console.WriteLine("133 + komplement von 72: {0}",133+~72);
            Console.WriteLine("3 + komplement von 2: {0}",3+~2);
            Console.WriteLine("1345 + komplement von 2: {0}",1345+~222);

        }
    }
}
