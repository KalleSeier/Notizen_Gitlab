﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _9_Listen
{
    internal class Program
    {
        static void Main(string[] args)
        {
            
            List<string> list = new List<string> {"a", "b", "c", "d", "f", "g", "h", "i", "j", "k", "l", };
            list.Add("m");
            list.Add("n");
            list.Add("f");
            List<List<string>> list2 = new List<List<string>>();
            list2.Add(list);
            list2.Add(new List<string>() { "a", "b", "c", "d", "f", "g"});
            list2.Add(new List<string>() { "f", "s", "3"});

            foreach(List<string> item in list2)
            {
                foreach(string l in list)
                {
                    Console.Write(l);
                }
            }
            Console.ReadKey();
            
        }
    }
}
