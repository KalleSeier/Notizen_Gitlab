﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _32_Methoden
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Funktion_Zahlen_Ausgeben();

            Console.ReadKey();
        }
        public static void Funktion_Zahlen_Ausgeben()
        {
            for(int i = 0; i<100;i++)
            {
                Console.WriteLine(i);
            }
        }
    }
}
