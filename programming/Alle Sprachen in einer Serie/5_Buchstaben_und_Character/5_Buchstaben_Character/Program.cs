﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5_Buchstaben_Character
{
    internal class Program
    {
        static void Main(string[] args)
        {
            char[] chars = new char[] {'E', 'i', 'n', ' ', 'C', 'h', 'a', 'r', 'i', 's', 't', ' ', 'e', 'i', 'n', ' ', 'B', 'y', 't', 'e', ' ', 'd', 'e', 'r', ' ', 'M', 'i', 't', 'h', 'i', 'l', 'f', 'e', ' ', 'd', 'e', 'r', ' ', 'A', 'S', 'C', 'I', 'I', ' ', 'T', 'a', 'b', 'e', 'l', 'l', 'e', 'g', 'e', 'l', 'e', 's', 'n', 'w', 'i', 'r', 'd'};
            foreach (char c in chars)
            {
                Console.Write(c);
            }
            Console.WriteLine("\n");
            byte[] zahlen = new byte[255];
            for (int i = 0; i < zahlen.Length; i++)
            {
                zahlen[i] = (byte)i;
            }
            foreach (byte b in zahlen)
            {
                Console.Write(Convert.ToChar(b));
            }
            Console.ReadKey();
        }
    }
}
