﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _11_arithmetische_Operationen
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int zahl1 = 5 + 6;
            int zahl2 = 5 - 3;
            int zahl3 = 5 * 4;
            int zahl4 = 50 / 5;
            int zahl5 = 5 % 3;
            Console.WriteLine(zahl1);
            Console.WriteLine(zahl2);
            Console.WriteLine(zahl3);
            Console.WriteLine(zahl4);
            Console.WriteLine(zahl5);
            Console.WriteLine("---------------------");
            zahl1++;
            Console.WriteLine(zahl1);
            zahl1--;
            Console.WriteLine(zahl1);
            Console.WriteLine("---------------------");
            zahl1 += 5;
            zahl2 -= 3;
            zahl3 *= 3;
            zahl4 /= 5;
            zahl5 %= 2;
            Console.WriteLine(zahl1);
            Console.WriteLine(zahl2);
            Console.WriteLine(zahl3);
            Console.WriteLine(zahl4);
            Console.WriteLine(zahl5);
            Console.WriteLine("---------------------");

            //wird zuerst zugewiesen und dann erhöht
            int zahl6 = 5;
            int zahl7 = zahl6++;
            Console.WriteLine(zahl6);
            Console.WriteLine(zahl7);

            Console.WriteLine("---------------------");

            //wird zuerst erhöht und dann zugewiesen
            int zahl8 = 4;
            int zahl9 = ++zahl8;
            Console.WriteLine(zahl8);
            Console.WriteLine(zahl9);


            Console.ReadKey();

        }
    }
}
