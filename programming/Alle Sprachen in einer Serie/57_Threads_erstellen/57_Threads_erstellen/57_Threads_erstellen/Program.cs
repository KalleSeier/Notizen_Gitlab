﻿namespace _57_Threads_erstellen
{
    using System.Threading;
    internal class Program
    {
        static void Main(string[] args)
        {
            object data = 1;
            object data2 = 2;

            ParameterizedThreadStart threadStart1 = new ParameterizedThreadStart(Write1_100);
            Thread thread1 = new Thread(threadStart1);
            thread1.Start(data);

            ParameterizedThreadStart threadStart2 = new ParameterizedThreadStart(Write1_100);
            Thread thread2 = new Thread(threadStart1);
            thread2.Start(data2);

            Console.ReadKey();
        }
        public static void Write1_100(object zaehler2)
        {
            int zaehler = (int)zaehler2;
            for(int i = 0; i <1000;i++)
            {
                Console.WriteLine("{0}:{1}", zaehler, i);
            }
        }
    }
}