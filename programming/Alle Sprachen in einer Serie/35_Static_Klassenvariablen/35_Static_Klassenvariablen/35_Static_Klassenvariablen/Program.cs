﻿using System;

namespace _35_Static_Klassenvariablen
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Schüler.schule);
            Schüler s1 = new Schüler("Ingried");
            //Console.WriteLine(s1.schule);
            Schüler.Get_Schule();

            Console.ReadKey();
        }

    }
    class Schüler
    {
        public static string schule = "HAKWEIZ";
        string name;
        public Schüler(string name)
        {
            this.name = name;
        }
        public static void Get_Schule()
        {
            Console.WriteLine(schule);
        }
    }
}
