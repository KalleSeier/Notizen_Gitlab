﻿namespace _35_static_Klassenvariablen_2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Tisch tisch1 = new Tisch("Tisch");
            Tisch tisch2 = new Tisch("Tisch");

            Console.WriteLine(Tisch.holzart);
            Tisch.Ausgabe();

            Console.ReadKey();
        }
        class Tisch
        {
            public static string holzart = "Fichte";
            public string name;
            public Tisch(string name)
            {
                this.name = name;
            }
            public static void Ausgabe()
            {
                Console.WriteLine(holzart);
            }
        }
    }
}