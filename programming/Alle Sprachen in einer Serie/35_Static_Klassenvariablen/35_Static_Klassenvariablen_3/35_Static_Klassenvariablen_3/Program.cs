﻿namespace _35_Static_Klassenvariablen_3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Kugelschreiber.Beschreibung();

            Console.WriteLine(Kugelschreiber.marke);

            Kugelschreiber kugelschreiber = new Kugelschreiber("blau");

            Console.WriteLine(kugelschreiber.ReturnFarbe());

            Console.ReadKey();
        }
    }
}