﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _35_Static_Klassenvariablen_3
{
    internal class Kugelschreiber
    {
        private string farbe;
        public static string marke = "lol";
        private static string beschreibung = "Ein Kugelschreiber kann schreiben sheeeeesh";
        public string Farbe { get { return farbe; } set { } }

        public Kugelschreiber(string farbe)
        {
            this.farbe = farbe;
        }
        
        public static void Beschreibung()
        {
            Console.WriteLine(beschreibung);
        }
        public string ReturnFarbe()
        {
            return Farbe;
        }
    }
}
