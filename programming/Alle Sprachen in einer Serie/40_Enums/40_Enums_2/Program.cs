﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _40_Enums_2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            NotenVergeben(Bewertung.Gut);

            Console.ReadKey();
        }
        public enum Bewertung
        {
            nichtGenügend,
            Genügend,
            Befriedigend,
            Gut,
            SehrGut
        }
        public static void NotenVergeben(Bewertung bewertung)
        {
            if(bewertung == Bewertung.nichtGenügend)
            {
                Console.WriteLine("Nicht Genügend");
            }
            else if(bewertung == Bewertung.Genügend)
            {
                Console.WriteLine("Genügend");
            }
            else if(bewertung==Bewertung.Befriedigend)
            {
                Console.WriteLine("Befriedigend");
            }
            else if(bewertung == Bewertung.Gut)
            {
                Console.WriteLine("Gut");
            }
            else if(bewertung == Bewertung.SehrGut)
            {
                Console.WriteLine("Sehr Gut");
            }
            else
            {
                Console.WriteLine("Error");
            }
        }
    }
}
