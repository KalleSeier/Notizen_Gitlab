﻿namespace _40_Enums
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Richtung richtung = new Richtung();
            richtung = ReturnEnum();

            if(richtung == Richtung.Oben)
            {
                Console.WriteLine("Gehe nach oben");
            }
            Console.ReadKey();
        }
        public enum Richtung
        {
            Links,Rechts,Oben,Unten

        }
        public static Richtung ReturnEnum()
        {
            return Richtung.Oben;
        }
    }
}