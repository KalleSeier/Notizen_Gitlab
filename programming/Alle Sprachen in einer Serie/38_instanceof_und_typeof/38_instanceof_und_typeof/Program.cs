﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _38_instanceof_und_typeof
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string name_h = "Huawai P30 Lite";
            double stromverbrauch_h = 13.54;
            string name_s = "Huawai P30 Lite";
            double stromverbrauch_s = 40.54;
            Handy handy = new Handy(name_h,stromverbrauch_h);
            Staubsauger staubsauger = new Staubsauger(name_s, stromverbrauch_s);
            Buttermesser buttermesser = new Buttermesser();
            List<object> Gegenstände = new List<object>() { handy, staubsauger,buttermesser };

            //handy.Start_Device();

            for(int i = 0;i<Gegenstände.Count;i++)
            {
                if(Gegenstände[i] is Elektrogeräte)
                {
                    Console.WriteLine("Gegenstand is von der gleichen Klasse oder von einer Unterklasse von Elektrogeräte");
                    Elektrogeräte e = (Elektrogeräte)Gegenstände[i];
                    e.Start_Device();
                }
            }
            Console.WriteLine("______________________________________");
            for (int i = 0; i < Gegenstände.Count; i++)
            {
                if (Gegenstände[i] is Ladebares_Gerät)
                {
                    Console.WriteLine("Gegenstand is genau von der Klasse Handy");
                    Elektrogeräte e = (Elektrogeräte)Gegenstände[i];
                    e.Start_Device();
                }
            }

            //for(int i = 0; i< 150;i++)
            //{
            //    Console.WriteLine(handy.Strom_Verbraucht);
            //    Thread.Sleep(1100);
            //}
            //handy.Aufladen();
            //for (int i = 0; i < 150; i++)
            //{
            //    Console.WriteLine(handy.Strom_Verbraucht);
            //    Thread.Sleep(1100);
            //}




            Console.ReadKey();
        }
    }
}
