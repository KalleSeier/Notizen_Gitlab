﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _38_instanceof_und_typeof
{
    internal abstract class Elektrogeräte
    {
        protected string name;
        protected double verbrauch_pro_minute;
        protected const int miliseconds_in_sec = 1000;
        protected double strom_verbraucht = 0;
        protected Thread count_eletricity;
        protected static readonly object Locker = new object();
        public string Name {get { return name; } set { } }
        public double Verbrauch_pro_minute { get { return verbrauch_pro_minute; } set { } }
        public int Milisec_in_min { get { return miliseconds_in_sec; }}
        public double Strom_Verbraucht { get { return strom_verbraucht; } }


        public Elektrogeräte(string name,double verbrauch_pro_minute)
        {
            this.name = name;
            this.verbrauch_pro_minute = verbrauch_pro_minute;
            count_eletricity = new Thread(() => Count_Electricity(verbrauch_pro_minute));
        }
        public void Start_Device()
        {
            count_eletricity.Start();
        }
        public void Pause_Device()
        {
            count_eletricity.Suspend();
        }
        virtual protected void Count_Electricity(double verbrauch_pro_minute)
        {
            while (true)
            {
                Thread.Sleep(miliseconds_in_sec);
                lock (Locker)
                {
                    strom_verbraucht += Verbrauch_pro_minute;
                }
            }
        }
        abstract public void Ausstecken();
    }
}
