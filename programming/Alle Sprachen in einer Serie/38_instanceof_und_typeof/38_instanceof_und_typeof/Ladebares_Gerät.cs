﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _38_instanceof_und_typeof
{
    internal interface Ladebares_Gerät
    {
        double Akkustand { get; set; }
        void Aufladen();
    }
}
