﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Collections;

namespace _38_instanceof_und_typeof
{
    internal class Handy : Elektrogeräte, Ladebares_Gerät
    {
        //private event ChangedEventHandler changed;
        protected double akkustand;
        public double Akkustand { get { return akkustand; } set { } }
        public Handy(string name,double verbrauch_pro_minute) : base(name,verbrauch_pro_minute)
        {
            akkustand = 0;
        }
        override public void Ausstecken()
        {
            base.Pause_Device();
            base.strom_verbraucht = 0;
        }
        public void Aufladen()
        {
            akkustand = 100;
            Console.WriteLine("Handy wurde aufgeladen");
        }
        override protected void Count_Electricity(double verbrauch_pro_minute)
        {
            while (true)
            {
                Thread.Sleep(miliseconds_in_sec);
                lock (Locker)
                {
                    strom_verbraucht += Verbrauch_pro_minute;
                    akkustand -= 1;
                }
            }
        }
    }
}
