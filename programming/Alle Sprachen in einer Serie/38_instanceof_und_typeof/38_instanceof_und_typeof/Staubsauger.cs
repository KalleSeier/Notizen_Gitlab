﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _38_instanceof_und_typeof
{
    internal class Staubsauger : Elektrogeräte
    {
        public Staubsauger(string name, double verbrauch_pro_minute) : base(name, verbrauch_pro_minute)
        {

        }
        override public void Ausstecken()
        {
            base.Pause_Device();
            base.strom_verbraucht = 0;
        }
    }
}
