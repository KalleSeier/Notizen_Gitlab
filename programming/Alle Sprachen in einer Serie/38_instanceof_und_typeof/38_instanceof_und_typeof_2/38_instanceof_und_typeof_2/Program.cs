﻿namespace _38_instanceof_und_typeof_2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Stein stein = new Stein();
            Marmor marmor = new Marmor();

            object test = marmor;

            if(test != null)
            {
                if(test.GetType() == typeof(Marmor))
                {
                    Console.WriteLine("test ist eine instanz von Marmor");
                }
                if(test is Stein)
                {
                    Console.WriteLine("test gehört der Klasse Stein oder einer Unterklasse davon an");
                }
            }

            

        }
    }
}