﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _16_while_Schleifen
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string passwort = "abc";
            bool ueberpruefung = false;

            while(!ueberpruefung)
            {
                Console.WriteLine("Passwort eingeben");
                if(Console.ReadLine() == passwort)
                {
                    ueberpruefung=true;
                    Console.WriteLine("Passwort korrekt");
                }
            }
            Console.ReadKey();
        }
    }
}
